package com.playtagon.pandarium_web.ethereum;

import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;

import org.junit.Assert;
import org.junit.Test;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.infura.InfuraHttpService;
import org.web3j.utils.Convert;
import org.web3j.utils.Convert.Unit;

public class EthereumTest {

	// @Test
	public void testGetbalance() throws InterruptedException, ExecutionException {
		String ethereumNode = "https://kovan.infura.io/fJpeb3NemsYt7wHKZMGu";
		Web3j web3j = Web3j.build(new HttpService());

		EthGetBalance ceotBalanceEth = web3j
				.ethGetBalance("0x00360d2b7D240Ec0643B6D819ba81A09e40E5bCd", DefaultBlockParameterName.LATEST)
				.sendAsync().get();

		Assert.assertNotNull(ceotBalanceEth.getResult());
	}

	@Test
	public void testSumBalance() throws InterruptedException, ExecutionException {
		String mainEthereumNode = "https://mainnet.infura.io/fJpeb3NemsYt7wHKZMGu";
		Web3j web3j = Web3j.build(new InfuraHttpService(mainEthereumNode));

		String[] wallets = { 
				"0x0e26169270d92ff3649461b55ca51c99703de59e", 
				"0x75771dedde9707fbb78d9f0dbdc8a4d4e7784794",
				"0x0d41f957181e584db82d2e316837b2de1738c477", 
				"0x11f42abcf0985e1fa5a961d846412f5a3d75b8a8",
				"0x79bd592415ff6c91cfe69a7f9cd091354fc65a18", 
				"0x7c00c9f0e7aed440c0c730a9bd9ee4f49de20d5c",
				"0xd387a6e4e84a6c86bd90c158c6028a58cc8ac459", 
				"0xbc1b89612b8c8e006929197569818e785e427bfb",
				"0xb3589bbd2781aad79cc2727fd5abb762b522a7a2", 
				"0x04b896ef1baa948e43e18fd43ee9124db549e7ee",
				"0x0429c8d18b916dffa9d3ac0bc56d34d9014456ef", 
				"0xb12609b50c56acae3b200ea259a246fd03d0f87e",
				"0x8ef740190bf0572ff1714036ad5d07ac247f4b41",
				"0xc179fbddc946694d11185d4e15dbba5fd0adac0a",
				"0x1d8df5e49b092427c954f8e8d863d7da27c003b9",
				"0x4b60b53a6edcd75f0730ecc22579df2079f210b0"
		};

		EthGetBalance balance = web3j
				.ethGetBalance(wallets[0], DefaultBlockParameterName.LATEST)
				.sendAsync().get();
		
		BigDecimal e = Convert.fromWei(BigDecimal.valueOf(balance.getBalance().floatValue()), Unit.ETHER);
		System.out.println("b: " + e.floatValue());
		
		float sum = 0;
		for (String wallet : wallets) {
			EthGetBalance ceotBalanceEth = web3j.ethGetBalance(wallet, DefaultBlockParameterName.LATEST).sendAsync()
					.get();
			float eth = Convert.fromWei(BigDecimal.valueOf(ceotBalanceEth.getBalance().floatValue()), Unit.ETHER)
					.floatValue();
			sum += eth;
		}

		System.out.println("Ethereum sum: " + sum);
	}
}
