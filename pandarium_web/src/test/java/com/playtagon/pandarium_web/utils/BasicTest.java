package com.playtagon.pandarium_web.utils;

import java.io.InputStream;
import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.zkoss.zats.mimic.Client;
import org.zkoss.zats.mimic.ComponentAgent;
import org.zkoss.zats.mimic.DefaultZatsEnvironment;
import org.zkoss.zats.mimic.DesktopAgent;
import org.zkoss.zats.mimic.ZatsEnvironment;

import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;

public class BasicTest {
	private static ZatsEnvironment env;
	protected static HazelcastInstance hz;

	@BeforeClass
	public static void init() {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("hazelcast_test.xml");
		Config config = new XmlConfigBuilder(is).build();
		hz = Hazelcast.newHazelcastInstance(config);
		HazelcastUtil.getInstance().setHazelcastInstance(hz);

		env = new DefaultZatsEnvironment("./src/main/webapp/WEB-INF");
		env.init("./src/main/webapp");
	}

	@AfterClass
	public static void end() {
		env.destroy();
		hz.shutdown();
	}

	@After
	public void after() {
		env.cleanup();
		hz.getMap("users").clear();
		hz.getMap("prelaunchUsers").clear();
		hz.getMap("admins").clear();
		hz.getMap("common_settings").clear();
	}

	public Client createNewClient() {
		return env.newClient();
	}

	public void asserts(WaitAction action, int milliseconds) {
		ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);
		exec.schedule(new Runnable() {
			public void run() {
				action.process();
			}
		}, milliseconds, TimeUnit.MILLISECONDS);
	}

	public String generateRandomEmail() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 10) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr + "@gmail.com";
	}

	protected static ComponentAgent getWindowComponentAgent(DesktopAgent desktop, String componentId) {
		return desktop.query("window").query(componentId);
	}

	@FunctionalInterface
	public interface WaitAction {
		void process();
	}
}
