package com.playtagon.pandarium_web.utils;

import org.zkoss.zats.mimic.ComponentAgent;
import org.zkoss.zats.mimic.DesktopAgent;
import org.zkoss.zul.Label;
import org.zkoss.zul.Slider;

public class MainWebsiteComponentGetter {

	public static ComponentAgent getEmailInput(DesktopAgent desktop) {
		return desktop.query("window").query("#email");
	}

	public static ComponentAgent getConfirmEmail(DesktopAgent desktop) {
		return desktop.query("window").query("#confirm");
	}

	public static Label getErrorLabel(DesktopAgent desktop) {
		return desktop.query("window").query("#error").as(Label.class);
	}

	public static Label getEmailStatusLabel(DesktopAgent desktop) {
		return desktop.query("window").query("#message").as(Label.class);
	}

	public static Label getReferralLinkLabel(DesktopAgent desktop) {
		return null;
	}

	public static Slider getFriendSlider(DesktopAgent desktop) {
		return null;
	}

	public static Label getFirstFriendThresholdLabel(DesktopAgent desktop) {
		return null;
	}

	public static Label getSecondFriendThresholdLabel(DesktopAgent desktop) {
		return null;
	}

	public static Label getThirdFriendThresholdLabel(DesktopAgent desktop) {
		return null;
	}

	public static Label getFourthFriendThresholdLabel(DesktopAgent desktop) {
		return null;
	}

}
