package com.playtagon.pandarium_web.utils;

import org.zkoss.zats.mimic.ComponentAgent;
import org.zkoss.zats.mimic.DesktopAgent;
import org.zkoss.zul.Label;

public class AdminPanelComponentGetter {

	// --------------------Login zul----------------------
	public static ComponentAgent getNameInput(DesktopAgent desktop) {
		return desktop.query("window").query("#name");
	}

	public static ComponentAgent getPassInput(DesktopAgent desktop) {
		return desktop.query("window").query("#pass");
	}

	public static ComponentAgent getLoginButton(DesktopAgent desktop) {
		return desktop.query("window").query("#login");
	}

	public static Label getLoginError(DesktopAgent desktop) {
		return desktop.query("window").query("#err").as(Label.class);
	}

	// ----------------------------------------------------

	// -----------------Admin panel---------------------------
	public static Label getFirstFriendCountThresholdLab(DesktopAgent desktop) {
		return desktop.query("#prelaunchParamsWin").query("#firstThresholdLab").as(Label.class);
	}
	
	public static Label getSecondFriendCountThresholdLab(DesktopAgent desktop) {
		return desktop.query("#prelaunchParamsWin").query("#secondThresholdLab").as(Label.class);
	}
	
	public static Label getThirdFriendCountThresholdLab(DesktopAgent desktop) {
		return desktop.query("#prelaunchParamsWin").query("#thirdThresholdLab").as(Label.class);
	}
	
	public static Label getFourthFriendCountThresholdLab(DesktopAgent desktop) {
		return desktop.query("#prelaunchParamsWin").query("#fourthThresholdLab").as(Label.class);
	}

	// ----------------------------------------------------
}
