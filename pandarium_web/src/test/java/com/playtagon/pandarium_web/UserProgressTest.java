package com.playtagon.pandarium_web;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Test;
import org.zkoss.zats.mimic.Client;
import org.zkoss.zats.mimic.ComponentAgent;
import org.zkoss.zats.mimic.DesktopAgent;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.models.PrelaunchRewardParams;
import com.playtagon.pandarium_cluster.services.AdminService;
import com.playtagon.pandarium_cluster.services.PrelaunchService;
import com.playtagon.pandarium_cluster.services.PrelaunchUserService;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration;
import com.playtagon.pandarium_web.utils.BasicTest;

public class UserProgressTest extends BasicTest {
	private final String testValidEmail = "test"
			+ ThreadLocalRandom.current().nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE) + "@test.mail.com";

	@Test
	public void testUserProgressLink() throws InterruptedException {
		savePelaunchRewardParameters();

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		ObjectId userId = registerUser(userDao, testValidEmail, null, true);

		PrelaunchUserService userService = new PrelaunchUserService(userDao);

		String privateToken = userService.getPrivateTokenByUserId(userId);

		Map<String, Object> args = new HashMap<>();
		args.put("token", privateToken);

		Client client = createNewClient();
		DesktopAgent desktop = client.connectAsIncluded("/user_progress.zul", args);

		ComponentAgent progressInfo = getWindowComponentAgent(desktop, "#userProgress");

		asserts(() -> {
			Assert.assertNotNull(progressInfo);
			Assert.assertTrue(progressInfo.getChildren().size() > 0);
		}, 1000);

		Thread.sleep(3000);
	}

	@Test
	public void testUserProgressInvalidLink() throws InterruptedException {
		savePelaunchRewardParameters();

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		registerUser(userDao, testValidEmail, null, true);

		String invalidPrivateToken = "892374932749invalidPrivateTokenlajlaidlkajslkj";

		Map<String, Object> args = new HashMap<>();
		args.put("token", invalidPrivateToken);

		Client client = createNewClient();
		DesktopAgent desktop = client.connectAsIncluded("/user_progress.zul", args);

		ComponentAgent progressInfo = getWindowComponentAgent(desktop, "#userProgress");

		asserts(() -> {
			Assert.assertNull(progressInfo);
		}, 1000);

		Thread.sleep(2000);
	}

	@Test
	public void testUserProgressRewards() throws InterruptedException {
		savePelaunchRewardParameters();

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		ObjectId userId = registerUser(userDao, testValidEmail, null, true);

		// Register user by referralLink
		// [5 users] - total invited
		// [3/5 users] - activated account
		registerUser(userDao, "invitedemail1@test.com", testValidEmail, true);
		registerUser(userDao, "invitedemail2@test.com", testValidEmail, true);
		registerUser(userDao, "invitedemail3@test.com", testValidEmail, true);
		registerUser(userDao, "invitedemail4@test.com", testValidEmail, false);
		registerUser(userDao, "invitedemail5@test.com", testValidEmail, false);

		PrelaunchUserService userService = new PrelaunchUserService(userDao);

		String privateToken = userService.getPrivateTokenByUserId(userId);

		Map<String, Object> args = new HashMap<>();
		args.put("token", privateToken);

		Client client = createNewClient();
		DesktopAgent desktop = client.connectAsIncluded("/user_progress.zul", args);

		ComponentAgent progressInfo = getWindowComponentAgent(desktop, "#userProgress");

		asserts(() -> {
			Assert.assertNotNull(progressInfo);
			Assert.assertTrue(progressInfo.getChildren().size() > 0);

			Label totalFriends = getWindowComponentAgent(desktop, "#labelTotal").as(Label.class);
			Label activeFriends = getWindowComponentAgent(desktop, "#labelActive").as(Label.class);
			Listbox listbox = desktop.query("window").query("#friends").as(Listbox.class);

			for (Listitem item : listbox.getItems()) {
				Listcell cell = (Listcell) item.getFirstChild();
				System.out.println(cell.getLabel());
			}

			Assert.assertEquals(totalFriends.getValue(), "5");
			Assert.assertEquals(activeFriends.getValue(), "3");
			Assert.assertEquals(listbox.getItemCount(), 5);
		}, 3000);

		Thread.sleep(5000);
	}

	private ObjectId registerUser(PrelaunchUserDao userDao, String email, String referralEmail, boolean activated) {
		PrelaunchUserRegistration registration = new PrelaunchUserRegistration(new ObjectId(), referralEmail);
		registration.setRegistrationDate(new Date());
		PrelaunchUser user = new PrelaunchUser(new ObjectId(), email, registration);
		userDao.save(user.getId(), user);

		if (activated) {
			PrelaunchService prelaunchService = new PrelaunchService(userDao, new CommonSettingsDao(hz));
			prelaunchService.activateUser(user.getId());
		}

		return user.getId();
	}

	private void savePelaunchRewardParameters() {
		PrelaunchRewardParams params = new PrelaunchRewardParams();
		params.setUserCountGettingPandas(50);
		params.setFriendCountThreshold(10);
		params.setRewardPandaCount(5);

		AdminService adminService = new AdminService(new CommonSettingsDao(hz));
		adminService.savePelaunchRewardParameters(params);
	}
}
