package com.playtagon.pandarium_web;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;

import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Test;
import org.zkoss.zats.mimic.Client;
import org.zkoss.zats.mimic.DesktopAgent;
import org.zkoss.zul.Vbox;

import com.hazelcast.map.AbstractEntryProcessor;
import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.services.PrelaunchService;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration.TokenType;
import com.playtagon.pandarium_web.utils.BasicTest;

public class ConfirmTest extends BasicTest {
	private final String testValidEmail = "test"
			+ ThreadLocalRandom.current().nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE) + "@test.mail.com";

	@Test
	public void testValidUserConfirmation() throws InterruptedException {
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		ObjectId userId = registerUser(userDao, testValidEmail);

		String confirmToken = PrelaunchService.generateTokenByEmail(TokenType.CONFIRM, testValidEmail);
		setConfirmTokenByEmal(userId, confirmToken);

		Map<String, Object> args = new HashMap<>();
		args.put("token", confirmToken);

		Client client = createNewClient();
		DesktopAgent desktop = client.connectAsIncluded("/confirm.zul", args);

		Vbox info = desktop.query("window").query("#info").as(Vbox.class);
		Assert.assertFalse(info.isVisible());

		asserts(() -> {
			Assert.assertTrue(info.isVisible());
		}, 3000);

		Thread.sleep(5000);
	}

	@Test
	public void testActiveUserConfirmation() throws InterruptedException {
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		ObjectId userId = registerUser(userDao, testValidEmail);
		String confirmToken = PrelaunchService.generateTokenByEmail(TokenType.CONFIRM, testValidEmail);
		setConfirmTokenByEmal(userId, confirmToken);

		PrelaunchService prelaunchService = new PrelaunchService(userDao, new CommonSettingsDao(hz));
		prelaunchService.activateUser(userId);

		Map<String, Object> args = new HashMap<>();
		args.put("token", confirmToken);

		Client client = createNewClient();
		DesktopAgent desktop = client.connectAsIncluded("/confirm.zul", args);

		Vbox info = desktop.query("window").query("#info").as(Vbox.class);
		Assert.assertFalse(info.isVisible());
		WaitAction action = () -> {
			Assert.assertTrue(info.isVisible());
		};
		asserts(action, 3000);

		Thread.sleep(5000);
	}

	private ObjectId registerUser(PrelaunchUserDao userDao, String email) {
		PrelaunchUserRegistration registration = new PrelaunchUserRegistration(null, testValidEmail);
		ObjectId id = new ObjectId();
		PrelaunchUser user = new PrelaunchUser(id, testValidEmail, registration);
		userDao.save(id, user);
		return id;
	}

	@SuppressWarnings("serial")
	private void setConfirmTokenByEmal(ObjectId userId, String token) {
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);

		userDao.getMap().executeOnKey(userId, new AbstractEntryProcessor<ObjectId, PrelaunchUser>() {

			@Override
			public Object process(Entry<ObjectId, PrelaunchUser> entry) {
				PrelaunchUser user = entry.getValue();
				user.getPrelaunchRegistration().setConfirmToken(token);
				entry.setValue(user);
				return null;
			}
		});
	}
}
