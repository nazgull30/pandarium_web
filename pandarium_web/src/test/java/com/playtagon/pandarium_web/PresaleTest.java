package com.playtagon.pandarium_web;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.infura.InfuraHttpService;

import com.playtagon.pandarium_cluster.contracts.PresaleCampaign;
import com.playtagon.pandarium_web.utils.BasicTest;

public class PresaleTest extends BasicTest {
	private PresaleCampaign presaleCampaign;

	@Before
	public void intiTest() {
		String ethereumNode = "https://ropsten.infura.io/fJpeb3NemsYt7wHKZMGu ";

		String contractAddress = "0xCD9b5843928af8333AC618E2741a45789Bd9f925";

		File file = new File(getClass().getClassLoader().getResource("wallets/simpleWallet.json").getFile());
		Credentials credentials = null;
		try {
			credentials = WalletUtils.loadCredentials("Playtagon12345", file);
		} catch (IOException | CipherException e) {
			e.printStackTrace();
		}

		Web3j web3j = Web3j.build(new InfuraHttpService(ethereumNode));

		presaleCampaign = PresaleCampaign.load(contractAddress, web3j, credentials, BigInteger.valueOf(1000),
				BigInteger.valueOf(470000));
	}

	@Test
	public void testRemainDuration() throws InterruptedException, ExecutionException {

		long durationMs = presaleCampaign.duration().sendAsync().get().longValue() * 1000;
		long startedAtAfterUtcMs = presaleCampaign.startedAt().sendAsync().get().longValue() * 1000;

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

		long currentDuration = calendar.getTimeInMillis() - startedAtAfterUtcMs;

		if (currentDuration < durationMs) {
			System.out.println("auction is going on");
		} else {
			System.out.println("auction is over");
		}
		
		Assert.assertEquals(durationMs, 0);
		Assert.assertEquals(startedAtAfterUtcMs, 0);
	}

}
