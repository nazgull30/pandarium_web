package com.playtagon.pandarium_web;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Test;
import org.zkoss.zats.mimic.Client;
import org.zkoss.zats.mimic.ComponentAgent;
import org.zkoss.zats.mimic.DesktopAgent;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;

import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.services.PrelaunchService;
import com.playtagon.pandarium_cluster.services.PrelaunchUserService;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration;
import com.playtagon.pandarium_web.utils.BasicTest;

public class RegistrationTest extends BasicTest {

	private final static String VIEW = "/registration.zul";

	private final String[] invalidEmails = { "test", "tes;*t@test.mail.ru", "test.mail.ru", "", "@test.mail.ru" };
	private final String testValidEmail = "test"
			+ ThreadLocalRandom.current().nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE) + "@test.mail.com";

	@Test
	public void testInvalidEmailFormat() {
		Client client = createNewClient();
		DesktopAgent desktop = client.connect(RegistrationTest.VIEW);

		ComponentAgent emailInput = getWindowComponentAgent(desktop, "#emailInput");
		ComponentAgent saveButton = getWindowComponentAgent(desktop, "#saveButton");

		Label errorLabel = getWindowComponentAgent(desktop, "#error").as(Label.class);
		Label messageLabel = getWindowComponentAgent(desktop, "#message").as(Label.class);

		for (String invalidEmail : invalidEmails) {
			emailInput.input(invalidEmail);
			saveButton.click();
			Assert.assertEquals("Invalid email. Check your input please.", errorLabel.getValue());
			Assert.assertTrue(messageLabel.getValue().isEmpty());
		}
	}

	@Test
	public void testSaveEmail() throws InterruptedException {
		Client client = createNewClient();
		DesktopAgent desktop = client.connect(RegistrationTest.VIEW);

		ComponentAgent emailInput = getWindowComponentAgent(desktop, "#emailInput");
		ComponentAgent saveButton = getWindowComponentAgent(desktop, "#saveButton");

		Label errorLabel = getWindowComponentAgent(desktop, "#error").as(Label.class);
		Label messageLabel = getWindowComponentAgent(desktop, "#message").as(Label.class);

		emailInput.input(testValidEmail);
		saveButton.click();

		Assert.assertEquals("Registering email...", messageLabel.getValue());

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);

		Awaitility.await().until(() -> userDao.count() == 1);
		Awaitility.await().until(() -> userDao.getUserIdByEmail(testValidEmail) != null);

		asserts(() -> {
			Assert.assertEquals("Your email has been saved! We sent you a confirmation email.",
					messageLabel.getValue());
			Assert.assertTrue(errorLabel.getValue().isEmpty());
		}, 3000);

		Thread.sleep(5000); // test should shutdown after checkResult
	}

	@Test
	public void testExistingEmail() {
		Client client = createNewClient();
		DesktopAgent desktop = client.connect(RegistrationTest.VIEW);

		ComponentAgent emailInput = getWindowComponentAgent(desktop, "#emailInput");
		ComponentAgent saveButton = getWindowComponentAgent(desktop, "#saveButton");

		Label errorLabel = getWindowComponentAgent(desktop, "#error").as(Label.class);
		Label messageLabel = getWindowComponentAgent(desktop, "#message").as(Label.class);

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);

		PrelaunchUserRegistration prelaunchRegistration = new PrelaunchUserRegistration(null, testValidEmail);
		ObjectId userId = new ObjectId();
		PrelaunchUser user = new PrelaunchUser(userId, testValidEmail, prelaunchRegistration);

		userDao.save(userId, user);

		emailInput.input(testValidEmail);
		saveButton.click();

		Assert.assertEquals("This email has already been registered", errorLabel.getValue());
		Assert.assertTrue(messageLabel.getValue().isEmpty());
		Assert.assertEquals(userDao.count(), 1L);
	}

	@Test
	public void testValidateIpAddr() {
		// save user with test email
		PrelaunchUserRegistration[] prelaunchRegistrations = {
				new PrelaunchUserRegistration(new ObjectId(), "test1@mail.ru"),
				new PrelaunchUserRegistration(new ObjectId(), "test2@mail.ru") };

		PrelaunchUser[] users = { new PrelaunchUser(new ObjectId(), "test1@mail.ru", prelaunchRegistrations[0]),
				new PrelaunchUser(new ObjectId(), "test2@mail.ru", prelaunchRegistrations[1]) };

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		for (PrelaunchUser user : users) {
			userDao.save(user.getId(), user);
		}

		Client client = createNewClient();
		DesktopAgent desktop = client.connect(RegistrationTest.VIEW);

		ComponentAgent emailInput = getWindowComponentAgent(desktop, "#emailInput");
		ComponentAgent saveButton = getWindowComponentAgent(desktop, "#saveButton");

		Label errorLabel = getWindowComponentAgent(desktop, "#error").as(Label.class);
		Label messageLabel = getWindowComponentAgent(desktop, "#message").as(Label.class);

		emailInput.input(testValidEmail);
		saveButton.click();

		Assert.assertEquals("Sorry, but you cannot register more then 2 emails.", errorLabel.getValue());
		Assert.assertTrue(messageLabel.getValue().isEmpty());
		Assert.assertEquals(userDao.count(), 2L);
	}

	@Test
	public void testOpenRegistrationWithActiveReferral() {
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);

		String inviterEmail = "inviter@testmail.com";
		PrelaunchUserRegistration inviterRegistration = new PrelaunchUserRegistration(null, inviterEmail);

		ObjectId userId = new ObjectId();

		PrelaunchUser inviter = new PrelaunchUser(userId, inviterEmail, inviterRegistration);
		userDao.save(userId, inviter);

		PrelaunchService prelaunchService = new PrelaunchService(userDao, new CommonSettingsDao(hz));
		prelaunchService.activateUser(userId);

		PrelaunchUserService userService = new PrelaunchUserService(userDao);

		String referralToken = userService.getReferralTokenByUserId(userId);

		System.out.println("referraltoken: " + referralToken);

		Map<String, Object> args = new HashMap<>();
		args.put("token", referralToken);

		Client client = createNewClient();
		DesktopAgent desktop = client.connectAsIncluded("/registration.zul", args);

		Hbox referralInfo = getWindowComponentAgent(desktop, "#referralInfo").as(Hbox.class);

		Assert.assertNotNull(referralInfo);
		Assert.assertTrue(referralInfo.isVisible());

		Label referralEmail = desktop.query("window").query("#referralEmail").as(Label.class);
		Assert.assertEquals(referralEmail.getValue(), inviterEmail);
	}

	@Test
	public void testOpenRegistrationWithInvalidReferral() {
		String invalidReferralToken = "439875invalid09438";

		Map<String, Object> args = new HashMap<>();
		args.put("token", invalidReferralToken);

		Client client = createNewClient();
		DesktopAgent desktop = client.connectAsIncluded("/registration.zul", args);

		ComponentAgent referralInfo = getWindowComponentAgent(desktop, "#referralInfo");

		ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);
		exec.schedule(new Runnable() {
			public void run() {
				Assert.assertNull(referralInfo);
			}
		}, 2, TimeUnit.SECONDS);
	}

}
