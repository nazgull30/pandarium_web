package com.playtagon.pandarium_web.adminpanel;

import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.zkoss.zats.mimic.Client;
import org.zkoss.zats.mimic.DefaultZatsEnvironment;
import org.zkoss.zats.mimic.DesktopAgent;
import org.zkoss.zats.mimic.ZatsEnvironment;
import org.zkoss.zul.Grid;

import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_web.utils.BasicTest;

public class AdminPanelApEmailTest extends BasicTest {
	private final int userCount = 100;

	@Before
	public void setUp() {
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		for (int i = 0; i < userCount; i++) {
			String randomEmail = generateRandomEmail();
			PrelaunchUser user = new PrelaunchUser(new ObjectId(), randomEmail, null);
			userDao.save(user.getId(), user);
		}

	}

	@Test
	public void testOpenAdminPanel() throws InterruptedException {
		ZatsEnvironment env = new DefaultZatsEnvironment("./src/test/resources/WEB-INF");
		env.init("./src/main/webapp");
		Client client = env.newClient();
		DesktopAgent desktop = client.connect("/adminpanel/adminpanel.zul");

		Grid emails = desktop.query("#hboxMain2").query("#emailsWin").query("div").query("#emails").as(Grid.class);

		asserts(() -> {
			Assert.assertEquals(emails.getChildren().size(), userCount);
		}, 3000);

		Thread.sleep(5000);

		env.cleanup();
	}

}
