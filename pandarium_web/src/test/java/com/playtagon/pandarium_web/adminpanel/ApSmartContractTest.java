package com.playtagon.pandarium_web.adminpanel;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;

import org.junit.Assert;
import org.junit.Test;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.infura.InfuraHttpService;

import com.playtagon.pandarium_cluster.contracts.PresaleCampaign;

public class ApSmartContractTest {

	@Test
	public void testStartContract() throws IOException, CipherException {
		File file = new File(getClass().getClassLoader().getResource("wallets/ropstenCeoWallet.json").getFile());
		Credentials credentials = WalletUtils.loadCredentials("Playtagon123", file);
		Web3j web3j = Web3j.build(new InfuraHttpService("https://ropsten.infura.io/fJpeb3NemsYt7wHKZMGu "));

		PresaleCampaign campaign = PresaleCampaign.load("0xCD9b5843928af8333AC618E2741a45789Bd9f925", web3j,
				credentials, BigInteger.valueOf(10000), BigInteger.valueOf(470000));

//		Assert.assertTrue(campaign.isValid());

	}
}
