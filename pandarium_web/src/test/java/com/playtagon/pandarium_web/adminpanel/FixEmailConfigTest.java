package com.playtagon.pandarium_web.adminpanel;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import org.apache.commons.io.IOUtils;

public class FixEmailConfigTest {

	// @Test
	// Just to fox email config test
	public void test() throws IOException {
		InputStream is = ClassLoader.getSystemResourceAsStream("test.txt");

		String string = IOUtils.toString(is);
		Scanner scanner = new Scanner(string);
		StringBuilder stringBuilder = new StringBuilder();

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			stringBuilder.append('"' + line + '"' + ", \n");
			// process the line
		}

		scanner.close();

		System.out.println(stringBuilder);
	}
}
