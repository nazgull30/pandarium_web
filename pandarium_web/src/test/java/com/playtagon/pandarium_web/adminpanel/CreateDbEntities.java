package com.playtagon.pandarium_web.adminpanel;

import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.mongodb.morphia.Datastore;

import com.playtagon.pandarium_cluster.utils.MorphiaUtil;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration;
import com.playtagon.pandarium_web.utils.BasicTest;

public class CreateDbEntities extends BasicTest {
	private final String username = "admin";
	private final String password = "123Playtagon456";

	// @Test
	public void createAdminAccountInMainDatabase() {
		RandomNumberGenerator rng = new SecureRandomNumberGenerator();
		byte[] salt = rng.nextBytes().getBytes();
		String hashedPassword = new Sha256Hash(password, salt, 1024).toBase64();

		Datastore datastore = MorphiaUtil.getInstance().getDatastore();

		// new AdminDao(datastore).createNewAdmin(username, hashedPassword, salt);
	}

	// @Test
	public void createAdminAccountInTestDatabase() {
		RandomNumberGenerator rng = new SecureRandomNumberGenerator();
		byte[] salt = rng.nextBytes().getBytes();
		String hashedPassword = new Sha256Hash(password, salt, 1024).toBase64();
		
		// new AdminDao(datastore).createNewAdmin(username, hashedPassword, salt);
	}
	
	@Test
	public void createUser() {
		System.out.println(hz.getMap("prelaunchUsers").size());
		PrelaunchUserRegistration registration = new PrelaunchUserRegistration();
		PrelaunchUser user = new PrelaunchUser(new ObjectId(), "nazg30@mail.ru", registration);
		hz.getMap("prelaunchUsers").set(user.getId(), user);
		
		
	}

}
