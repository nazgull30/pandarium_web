package com.playtagon.pandarium_web.adminpanel;

import java.io.StringWriter;
import java.util.Random;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Test;
import org.zkoss.zats.mimic.Client;
import org.zkoss.zats.mimic.DefaultZatsEnvironment;
import org.zkoss.zats.mimic.DesktopAgent;
import org.zkoss.zats.mimic.ZatsEnvironment;
import org.zkoss.zul.Label;

import com.playtagon.pandarium_cluster.dao.AdminDao;
import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.models.PrelaunchRewardParams;
import com.playtagon.pandarium_cluster.services.PrelaunchService;
import com.playtagon.pandarium_entities.CommonSettings;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration;
import com.playtagon.pandarium_web.utils.AdminPanelComponentGetter;
import com.playtagon.pandarium_web.utils.BasicTest;

public class AdminPanelTest extends BasicTest {
	private final String username = "admin";
	private final String password = "123Playtagon456";

	@Test
	public void testLoginAdminPanelSuccessfull() {
		createAdminInDb();

		Client client = createNewClient();
		DesktopAgent desktop = client.connect("/adminpanel/adminpanel_login.zul");
		AdminPanelComponentGetter.getNameInput(desktop).input(username);
		AdminPanelComponentGetter.getPassInput(desktop).input(password);
		AdminPanelComponentGetter.getLoginButton(desktop).click();

		Assert.assertTrue(AdminPanelComponentGetter.getLoginError(desktop).getValue().isEmpty());
	}

	@Test
	public void testLoginAdminPanelUnsuccessfull() {
		createAdminInDb();

		Client client = createNewClient();
		DesktopAgent desktop = client.connect("/adminpanel/adminpanel_login.zul");
		AdminPanelComponentGetter.getNameInput(desktop).input("admin");
		AdminPanelComponentGetter.getPassInput(desktop).input("right");
		AdminPanelComponentGetter.getLoginButton(desktop).click();

		Assert.assertFalse(AdminPanelComponentGetter.getLoginError(desktop).getValue().isEmpty());
	}

	@Test
	public void testOpenAdminPanelWhenPrelaunchParametersExist() throws JAXBException {
		createPrelaunchRewardParams();

		ZatsEnvironment env = new DefaultZatsEnvironment("./src/test/resources/WEB-INF");
		env.init("./src/main/webapp");
		Client client = env.newClient();
		DesktopAgent desktop = client.connect("/adminpanel/adminpanel.zul");

		Assert.assertEquals(
				desktop.query("#prelaunchParamsWin").query("#userCountGettingPandasLab").as(Label.class).getValue(),
				"50");
		Assert.assertEquals(
				desktop.query("#prelaunchParamsWin").query("#friendCountThresholdLab").as(Label.class).getValue(),
				"100");
		Assert.assertEquals(
				desktop.query("#prelaunchParamsWin").query("#rewardPandaCountLab").as(Label.class).getValue(), "5");
		env.cleanup();
		// System.out.println();
	}

	@Test
	public void testChangePrelaunchParameters() {
		ZatsEnvironment env = new DefaultZatsEnvironment("./src/test/resources/WEB-INF");
		env.init("./src/main/webapp");
		Client client = env.newClient();
		DesktopAgent desktop = client.connect("/adminpanel/adminpanel.zul");

		desktop.query("#prelaunchParamsWin").query("#userCountGettingPandas").input(2);
		desktop.query("#prelaunchParamsWin").query("#friendCountThreshold").input(5);
		desktop.query("#prelaunchParamsWin").query("#rewardPandaCount").input(10);

		Assert.assertEquals(
				desktop.query("#prelaunchParamsWin").query("#userCountGettingPandasLab").as(Label.class).getValue(),
				"2");
		Assert.assertEquals(
				desktop.query("#prelaunchParamsWin").query("#friendCountThresholdLab").as(Label.class).getValue(),
				"5");
		Assert.assertEquals(
				desktop.query("#prelaunchParamsWin").query("#rewardPandaCountLab").as(Label.class).getValue(), "10");

		env.cleanup();
	}

	@Test
	public void testSavePrelaunchParameters() {
		ZatsEnvironment env = new DefaultZatsEnvironment("./src/test/resources/WEB-INF");
		env.init("./src/main/webapp");
		Client client = env.newClient();
		DesktopAgent desktop = client.connect("/adminpanel/adminpanel.zul");

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		PrelaunchService prelaunchService = new PrelaunchService(userDao, new CommonSettingsDao(hz));

		desktop.query("#prelaunchParamsWin").query("#userCountGettingPandas").input(2);
		desktop.query("#prelaunchParamsWin").query("#friendCountThreshold").input(5);
		desktop.query("#prelaunchParamsWin").query("#rewardPandaCount").input(10);
		desktop.query("#prelaunchParamsWin").query("#save").click();

		PrelaunchRewardParams params = prelaunchService.getRewardParams();

		Assert.assertEquals(params.getUserCountGettingPandas(), 2);
		Assert.assertEquals(params.getFriendCountThreshold(), 5);
		Assert.assertEquals(params.getRewardPandaCount(), 10);

		env.cleanup();
	}

	@Test
	public void testRegisteredUsers() {
		Integer userCount = 10;
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);

		for (int i = 0; i < userCount; i++) {
			ObjectId id = new ObjectId();
			PrelaunchUser user = new PrelaunchUser();
			user.setId(id);
			userDao.save(id, user);
		}

		ZatsEnvironment env = new DefaultZatsEnvironment("./src/test/resources/WEB-INF");
		env.init("./src/main/webapp");
		Client client = env.newClient();
		DesktopAgent desktop = client.connect("/adminpanel/adminpanel.zul");

		Label registeredUsers = desktop.query("#analyticsWin").query("#registeredUsersLab").as(Label.class);
		Assert.assertEquals(registeredUsers.getValue(), userCount.toString());

		env.cleanup();
	}

	@Test
	public void testTotalPromotedPandas() throws JAXBException {
		createPrelaunchRewardParams();

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);

		PrelaunchUserRegistration firstInviterPr = new PrelaunchUserRegistration();
		firstInviterPr.setId(new ObjectId());
		PrelaunchUser firstInviter = new PrelaunchUser(new ObjectId(), "nazgull30@gmail.com", firstInviterPr);
		userDao.save(firstInviter.getId(), firstInviter);

		int firstInviterFriendsCount = 20; // 0 pandas
		for (int i = 0; i < firstInviterFriendsCount; i++) {
			PrelaunchUserRegistration inveteePr = new PrelaunchUserRegistration(new ObjectId(),
					firstInviter.getEmail());
			String randomMail = generateRandomString() + "@gmail.com";
			PrelaunchUser invetee = new PrelaunchUser(new ObjectId(), randomMail, inveteePr);
			userDao.save(invetee.getId(), invetee);
		}

		PrelaunchUserRegistration secondInviterPr = new PrelaunchUserRegistration();
		PrelaunchUser secondInviter = new PrelaunchUser(new ObjectId(), "pavel@mail.cru", secondInviterPr);
		userDao.save(secondInviter.getId(), secondInviter);

		int secondInviterFriendsCount = 200; // 5 pandas
		for (int i = 0; i < secondInviterFriendsCount; i++) {
			PrelaunchUserRegistration inveteePr = new PrelaunchUserRegistration(new ObjectId(),
					secondInviter.getEmail());
			String randomMail = generateRandomString() + "@gmail.com";
			PrelaunchUser invetee = new PrelaunchUser(new ObjectId(), randomMail, inveteePr);
			userDao.save(invetee.getId(), invetee);
		}

		ZatsEnvironment env = new DefaultZatsEnvironment("./src/test/resources/WEB-INF");
		env.init("./src/main/webapp");
		Client client = env.newClient();
		DesktopAgent desktop = client.connect("/adminpanel/adminpanel.zul");

		Label promotedPandas = desktop.query("#analyticsWin").query("#totalPandasLab").as(Label.class);
		Assert.assertEquals(promotedPandas.getValue(), "5");

		env.cleanup();
	}

	private void createAdminInDb() {
		RandomNumberGenerator rng = new SecureRandomNumberGenerator();
		byte[] salt = rng.nextBytes().getBytes();
		String hashedPassword = new Sha256Hash(password, salt, 1024).toBase64();

		new AdminDao(hz).createNewAdmin(username, hashedPassword, salt);
	}

	private void createPrelaunchRewardParams() throws JAXBException {
		PrelaunchRewardParams params = new PrelaunchRewardParams();
		params.setUserCountGettingPandas(50);
		params.setFriendCountThreshold(100);
		params.setRewardPandaCount(5);
		CommonSettings commonSettings = new CommonSettings("prelaunchRewardParams", getXmlFromParams(params));

		CommonSettingsDao settingsDao = new CommonSettingsDao(hz);
		settingsDao.save(commonSettings.getKey(), commonSettings);
	}

	private String getXmlFromParams(PrelaunchRewardParams params) throws JAXBException {
		StringWriter stringXml = new StringWriter();
		JAXBContext jaxbContext = JAXBContext.newInstance(PrelaunchRewardParams.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.marshal(params, stringXml);
		return stringXml.toString();
	}

	private String generateRandomString() {

		int leftLimit = 97; // letter 'a'
		int rightLimit = 122; // letter 'z'
		int targetStringLength = 5;
		Random random = new Random();
		StringBuilder buffer = new StringBuilder(targetStringLength);
		for (int i = 0; i < targetStringLength; i++) {
			int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
			buffer.append((char) randomLimitedInt);
		}
		String generatedString = buffer.toString();
		return generatedString;
	}
}
