function internalValidateOpinionLeaderInfo(name, email, description) {
	console.log("internalValidateOpinionLeaderInfo");
	
	if(name == null || name == "") {
		return "Type your name.";
	}
	
	if(description == null || description == "") {
		return "Type description.";
	}
	
	return internalValidateEmail(email);
}

function internalValidateEmail(email) {
	var rules = {
			required : true,
			email : true
		};
		console.log("start validating");

		var result = approve.value(email, rules);

		var hasErrors = result.errors.length > 0;
		if (hasErrors) {
			return "Invalid email. Check your input please.";
		}

		var emailProvider = email.split("@")[1];
		if (badEmails.includes(emailProvider)) {
			return "Very bad email provider!!!";
		}
		
		return "";
}

