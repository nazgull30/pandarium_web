abi_presale_contract = [ {
	"constant" : true,
	"inputs" : [],
	"name" : "duration",
	"outputs" : [ {
		"name" : "",
		"type" : "uint256"
	} ],
	"payable" : false,
	"stateMutability" : "view",
	"type" : "function"
}, {
	"constant" : false,
	"inputs" : [ {
		"name" : "totalPandas",
		"type" : "uint16"
	}, {
		"name" : "durationInMinutes",
		"type" : "uint256"
	}, {
		"name" : "pandaPrice",
		"type" : "uint256"
	} ],
	"name" : "start",
	"outputs" : [ {
		"name" : "status",
		"type" : "bool"
	} ],
	"payable" : false,
	"stateMutability" : "nonpayable",
	"type" : "function"
}, {
	"constant" : true,
	"inputs" : [],
	"name" : "initialPresalePandas",
	"outputs" : [ {
		"name" : "",
		"type" : "uint16"
	} ],
	"payable" : false,
	"stateMutability" : "view",
	"type" : "function"
}, {
	"constant" : true,
	"inputs" : [ {
		"name" : "",
		"type" : "address"
	} ],
	"name" : "addressPandasCount",
	"outputs" : [ {
		"name" : "",
		"type" : "uint16"
	} ],
	"payable" : false,
	"stateMutability" : "view",
	"type" : "function"
}, {
	"constant" : true,
	"inputs" : [],
	"name" : "pandasLeft",
	"outputs" : [ {
		"name" : "",
		"type" : "uint16"
	} ],
	"payable" : false,
	"stateMutability" : "view",
	"type" : "function"
}, {
	"constant" : true,
	"inputs" : [],
	"name" : "owner",
	"outputs" : [ {
		"name" : "",
		"type" : "address"
	} ],
	"payable" : false,
	"stateMutability" : "view",
	"type" : "function"
}, {
	"constant" : true,
	"inputs" : [ {
		"name" : "wallet",
		"type" : "address"
	} ],
	"name" : "getPandasCountFor",
	"outputs" : [ {
		"name" : "pandasCount",
		"type" : "uint16"
	} ],
	"payable" : false,
	"stateMutability" : "view",
	"type" : "function"
}, {
	"constant" : true,
	"inputs" : [],
	"name" : "priceForOnePanda",
	"outputs" : [ {
		"name" : "",
		"type" : "uint256"
	} ],
	"payable" : false,
	"stateMutability" : "view",
	"type" : "function"
}, {
	"constant" : true,
	"inputs" : [],
	"name" : "startedAt",
	"outputs" : [ {
		"name" : "",
		"type" : "uint256"
	} ],
	"payable" : false,
	"stateMutability" : "view",
	"type" : "function"
}, {
	"constant" : false,
	"inputs" : [ {
		"name" : "newOwner",
		"type" : "address"
	} ],
	"name" : "transferOwnership",
	"outputs" : [],
	"payable" : false,
	"stateMutability" : "nonpayable",
	"type" : "function"
}, {
	"payable" : true,
	"stateMutability" : "payable",
	"type" : "fallback"
}, {
	"anonymous" : false,
	"inputs" : [ {
		"indexed" : false,
		"name" : "wallet",
		"type" : "address"
	} ],
	"name" : "PandaSold",
	"type" : "event"
}, {
	"anonymous" : false,
	"inputs" : [],
	"name" : "SoldOut",
	"type" : "event"
} ]