var metamaskInstalled = false;
var networkValid = false;
var account;
var web3;
var presale;
var onePandaPriceWei;

function listenForAccountUpdates() {
	if (web3.eth.accounts[0] !== account) {
    		account = web3.eth.accounts[0]
    		reloadMyData();
  	}
}

function init(address) {
	if (typeof web3 !== 'undefined') {
		console.log('MetaMask!')
		web3 = new Web3(web3.currentProvider)
		
		metamaskInstalled = true
		
		web3.version.getNetwork((err, netId) => {
			switch (netId) {
		    case "1":
		    		console.log('This is mainnet')
			    alert('Please switch to Ropsten test network in your Metamask settings')
		        break
		    	case "2":
		      	console.log('This is the deprecated Morden test network.')
		      	alert('Please switch to Ropsten test network in your Metamask settings')
		      	break
			case "3":
		    		console.log('This is the Ropsten test network.')
		    		
		    		networkValid = true
		    		
		    		account = web3.eth.accounts[0]
				console.log(account);
		
				setInterval(listenForAccountUpdates, 100);
				
				initContract(address);
				reloadMyData();
				
				break
			default: 
				console.log('This is an unknown network.')
				alert('Please switch to Ropsten test network in your Metamask settings')
				break
			}
		})
		
	} else {
		console.log('No web3? You should consider trying MetaMask!')
		web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"))
	}
}

function initContract(address) {
	console.log("init contract, address: " + address);
	var presaleContract = web3.eth.contract(abi_presale_contract);
	presale = presaleContract.at(address);
	
	presale.startedAt(function(err, startedAtUtcSec){
		if(err != null) {
			alert("Error :( " + error)
		} else {
			var startedDate = new Date(0); 
			startedDate.setUTCSeconds(startedAtUtcSec);
			console.log("started at: " + startedDate);
			
			presale.duration(function(err, dur){
				if(err != null) {
					alert("Error :( " + error)
				} else {
					var now = new Date();
					var endDate = new Date(0);
					endDate.setUTCSeconds(startedAtUtcSec + dur);
					
					console.log("now: " + now + ", endDate: " + endDate);
					
					// auction is in progress
					if(now < endDate) {
						var remainDate = now - startedDate - durationDate;
						zk.Widget.$("$leftTime").setValue(remainDate.toISOString().substr(11, 8));
					} else {
						zk.Widget.$("$leftTime").setValue("Sale is over");
					}
				}
			} );
		}
	} );
	
	
    presale.priceForOnePanda(function(err, result){
		if(err != null) {
			alert("Error :( " + error)
		} else {
			onePandaPriceWei = result.toNumber(); 
			var onePandaPriceETH =  web3.fromWei(onePandaPriceWei, "ether"); 
			zk.Widget.$("$pandaPriceETH").setValue(onePandaPriceETH);
			console.log("Price for one panda: " + onePandaPriceETH);
		}
	} );
		
	presale.pandasLeft(function(err, result){
		if(err != null) {
			alert("Error :( " + error);
		} else {
			zk.Widget.$("$pandaRemainCount").setValue(result);
			console.log("Panda left: " + result);
		}
	});

}

function reloadMyData() {
	console.log("my account: " + account);
	presale.getPandasCountFor(account, (function(err, result){
		if(err != null) {
			alert("Error :( " + error);
		} else {
			zk.Widget.$("$myPandas").setValue(result);
			console.log("My pandas: " + result);
		}
	}));
}

function buy() {
	var buyPandaCount = zk.Widget.$("$buyPandaCount").getValue();
	if(buyPandaCount == 0) {
		alert("Input buyPandaCount");
		return;
	} 

	var weiAmount = buyPandaCount * onePandaPriceWei;

	console.log(weiAmount);
	var send = web3.eth.sendTransaction( {from:account, to:address, value:weiAmount, gas: 50000}, function(err, transactionHash) {
		if(!err) {
			console.log("SUCCESS!! " + transactionHash);
		} else {
			alert("Error :( " + err);
		}
	});
}

function buyPanda() {
	if (!metamaskInstalled) {
		alert("You can't use MetaMask to buy pandas.")
	} else if (!networkValid) {
		alert("Please switch to Ropsten test network in your Metamask settings")
	} else if (account === undefined) {
		alert("Please unlock your metamask wallet")
	} else {
		alert("You'll be able to buy pandas as soon as we implement required functionality!")
	}
}