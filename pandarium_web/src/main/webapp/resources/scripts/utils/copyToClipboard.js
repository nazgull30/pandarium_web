function copyLinkToClipboard() {
	var textArea = document.createElement("textarea");

	textArea.style.opacity = '0';
	textArea.value = zk.Widget.$('$refferalLink').getValue();

	document.body.appendChild(textArea);
	textArea.select();

	try {
		var successful = document.execCommand('copy');
		if (successful) {
			zk.Widget.$('$copyMessage').setSclass('z-label pop-up showed');

			setTimeout(function() {
				zk.Widget.$('$copyMessage').setSclass('z-label pop-up');
			}, 5000);
		}
		var msg = successful ? 'successful' : 'unsuccessful';
		console.log('Copying text command was ' + msg + " " + textArea.value);
	} catch (err) {
		console.log('Oops, unable to copy ' + err);
	}

	document.body.removeChild(textArea);
}