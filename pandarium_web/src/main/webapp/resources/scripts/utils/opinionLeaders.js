function showLeaderSendingStatus() {
	$("#saveOpinionLeaderBtn").hide();
	$("#opinionLeaderOpRes").css("form-alert");
	$("#opinionLeaderOpRes").text("Saving you're email...");
}

function validateOpinionLeaderForm() {
	console.log("validateOpinionLeaderForm");

	var name = $("#leaderName").val();
	var email = $("#leaderEmail").val();
	var description = $("#leaderDescr").val();

	var error = internalValidateOpinionLeaderInfo(name, email, description);

	if (error != "") {
		showOpinionLeaderInfo(error, true);
		event.stop(); // stop sending event to the server
	}
	return false;
}

function showLeaderSendResultStatus(error) {
	console.log("showLeaderSendResultStatus, success: " + error);
	$("#saveOpinionLeaderBtn").show();
	var mes = error ? "Your email has been already registered"
			: "Your email has been successfully added";
	showOpinionLeaderInfo(mes, error);
}

function showOpinionLeaderInfo(mes, error) {
	var style = error ? "form-alert form-alert__error" : "form-alert";
	$("#opinionLeaderOpRes").css(style);
	$("#opinionLeaderOpRes").text(mes);
	$("#opinionLeaderOpRes").fadeIn('100').delay('3000').fadeOut('200');
}
