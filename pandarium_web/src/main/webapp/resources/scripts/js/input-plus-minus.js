$('.input-plus').click(function (e) {
    e.preventDefault();
    field = $(this).parent().find('[name=quantity]');
    max = $(field).attr('max');
    var currentVal = parseInt(field.val());
    if (!isNaN(currentVal)) {
        if (currentVal < max) {
            field.val(currentVal + parseInt(field.attr('step')));
        } 
    } else {
        field.val(currentVal.attr('min'));
    }
    field.trigger('change');
});

$('.input-minus').click(function (e) {
    e.preventDefault();
    field = $(this).parent().find('[name=quantity]');
    min = $(field).attr('min');
    var currentVal = parseInt(field.val());
    if (!isNaN(currentVal) && currentVal > min) {
        field.val(currentVal - parseInt(field.attr('step')));
    }
    field.trigger('change');
});