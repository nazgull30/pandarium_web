var gulp = require('gulp'),
    sass = require('gulp-sass'),
    clean = require('gulp-clean'),
    watch = require('gulp-watch'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css'),
    rename = require("gulp-rename");
 
gulp.task('default', function() {
    gulp.watch('scss/*.scss', ['styles']);
});

gulp.task('styles', function() {
    gulp.src('scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('css/'))
});

gulp.task('prefixer', function() {
    gulp.src('css/style.css')
        .pipe(autoprefixer({
            browsers: ['last 20 versions','Safari >= 10.0','Chrome >=29','ie >= 10'],
            cascade: false
        }))
        .pipe(gulp.dest('css/'))
});

/**
gulp.task('unite', function() {
    return gulp.src(['fonts.css','css/bootstrap.css','css/normalize.css','css/style.css','css/jquery.fancybox.min.css'])
        .pipe(concat('bundle.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('css/'));
});
**/

gulp.task('copyjs', ['cleanjs'], function() {
    return gulp.src(mainBowerFiles('**/*.js'))
        .pipe(gulp.dest('libs'))
});

gulp.task('copycss', ['cleancss'], function() {
    return gulp.src(mainBowerFiles('**/*.css'))
        .pipe(gulp.dest('css'))
});

/*clean*/
gulp.task('cleanjs', function (path) {
    return gulp.src(['libs/*'], {read: false})
        .pipe(clean());
});

gulp.task('cleancss', function (path) {
    return gulp.src(['css/*', ], {read: false})
        .pipe(clean());
});