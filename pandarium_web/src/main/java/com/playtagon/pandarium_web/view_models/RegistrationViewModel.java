package com.playtagon.pandarium_web.view_models;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IExecutorService;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.services.PrelaunchUserService;
import com.playtagon.pandarium_cluster.tasks.RegisterUserTask;
import com.playtagon.pandarium_cluster.tasks.RegisterUserTask.RegisterUserError;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;
import com.playtagon.pandarium_web.checkers.EmailAndIpChecker;
import com.playtagon.pandarium_web.utils.LongOperation;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RegistrationViewModel {
	private static final String localhostAddr = "0:0:0:0:0:0:0:1";

	@Getter
	private String error;
	@Getter
	private String referralEmail;
	@Getter
	private boolean emailRegistered;

	@Getter
	private boolean registeringEmail;

	@Getter
	private int userCount;
	
	@Init
	public void init(@ExecutionArgParam("token") String token) {
		this.error = "";

		HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		PrelaunchUserService userService = new PrelaunchUserService(userDao);
		userCount = userDao.count();
		
		if (token != null) {
			String inviterEmail = userService.getInviterEmailByReferralTokenAndIfActive(userDao, token);

			if (inviterEmail != null) {
				referralEmail = inviterEmail;
			} else {
				Executions.sendRedirect("404.zul");
			}

			log.info("referral: " + referralEmail);
		}
	}

	@AfterCompose
	public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
		String animName = "sad";
		Clients.evalJavaScript("showAnimation('" + animName + "')");
	}

	@Command
	@NotifyChange({ "registeringEmail", "emailRegistered" })
	public void saveEmail(@BindingParam("email") String email) {
		if (!validateEmail(email))
			return;

		log.info("Save emal");

		String referralEmail = this.referralEmail;
		registeringEmail = true;

		LongOperation operation = new LongOperation() {

			@Override
			protected void execute() throws InterruptedException {
				HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
				IExecutorService executorService = hz.getExecutorService("default");
				Future<RegisterUserError> future = executorService.submit(new RegisterUserTask(email, referralEmail));

				try {
					RegisterUserError error = future.get();
					activate();
					processResult(error, email);
					deactivate();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}

			}
		};
		operation.start();
	}

	private boolean validateEmail(String email) {
		log.info("validate email: " + email);

		EmailAndIpChecker validator = new EmailAndIpChecker();
		boolean emailExists = validator.emailExists(email);
		if (emailExists) {
			String error = "This email has already been registered";
			Clients.evalJavaScript("showError('" + error + "')");
			return false;
		}

		return true;
	}

	private void processResult(RegisterUserError error, String email) {
		switch (error) {
		case NONE:
			this.error = "Your email has been saved! We sent you a confirmation email.";
			log.info("post changed");
			emailRegistered = true;
			String animName = "happy";
			Clients.evalJavaScript("showAnimation('" + animName + "')");
			BindUtils.postNotifyChange(null, null, this, "emailRegistered");
			break;
		case TRANSFER_EMAIL_ERROR:
			this.error = "Some problems occured when we tried to send you confirmation letter";
			break;
		case EMAIL_ALREADY_REGISTERED:
			this.error = "This email has already been registered";
			log.info("Email " + email + "already registered on MailChimp");
			break;
		case UNKNOWN:
			this.error = "Unknown error";
			break;
		default:
			break;
		}
		registeringEmail = false;

		BindUtils.postNotifyChange(null, null, this, "error");
		BindUtils.postNotifyChange(null, null, this, "registeringEmail");

		System.out.println("Register -> update error, " + error);
	}

}
