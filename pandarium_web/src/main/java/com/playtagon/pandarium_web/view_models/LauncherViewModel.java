package com.playtagon.pandarium_web.view_models;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;

import com.playtagon.pandarium_web.routing.Router;
import com.playtagon.pandarium_web.routing.RouterFactory;

public class LauncherViewModel {
	private RouterFactory routerFactory;
	private Router router;

	@Init
	public void init() {
		routerFactory = new RouterFactory();
		routerFactory.addRoute("/", "registration.html");
		routerFactory.addRoute("/reg", "registration.html");
		routerFactory.addRoute("/reg/{token}", "registration.html");
		routerFactory.addRoute("/progress/{token}", "referrals.html");
		routerFactory.addRoute("/presale/{token}", "user_presale.zul");
		routerFactory.addRoute("/confirm/{token}", "confirm.html");
		routerFactory.addRoute("/progress", "404.zul");
		routerFactory.addRoute("/presale", "404.zul");
		routerFactory.addRoute("/confirm", "404.zul");
		routerFactory.addRoute("/404", "404.zul");
	}

	@AfterCompose
	public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
		Component contentHolder = view.getFellow("contentHolder");
		router = routerFactory.createRouter(contentHolder);
	}

	@Command
	public void goToDefaultRoute() {		
		String cookiesToken = getPrivateTokenFromCookies();
		if (cookiesToken == null) {
			router.goTo("/");
		} else {
			String privateToken = cookiesToken;
			router.goTo("/progress/" + privateToken);
		}
	}

	@Command
	public void onHashChange(@BindingParam("url") String url) {
		router.dispatch(url);
	}
	
	private String getPrivateTokenFromCookies() {
		Cookie[] cookies = ((HttpServletRequest) Executions.getCurrent().getNativeRequest()).getCookies();

		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("user"))
				return cookie.getValue();
		}
		return null;
	}
}
