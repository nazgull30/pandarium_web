package com.playtagon.pandarium_web.view_models;

import org.bson.types.ObjectId;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.util.Clients;

import com.hazelcast.core.HazelcastInstance;
import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;
import com.playtagon.pandarium_entities.CommonSettings;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration.TokenType;

public class UserPresaleViewModel {

	@Init
	public void init(@ContextParam(ContextType.DESKTOP) Desktop desktop, @ExecutionArgParam("token") String token) {

		if (token == null)
			return;

		HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		ObjectId userId = userDao.getUserIdByToken(TokenType.PRESALE, token);
		if (userId == null)
			return;

		CommonSettingsDao settingsDao = new CommonSettingsDao(hz);
		CommonSettings contractAddress = settingsDao.getById("presaleContractAddress");

		if (contractAddress == null)
			return;

		Clients.evalJavaScript("init('" + contractAddress.getValue() + "')");
	}

}
