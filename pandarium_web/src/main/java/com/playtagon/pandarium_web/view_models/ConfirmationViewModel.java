package com.playtagon.pandarium_web.view_models;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IExecutorService;
import com.playtagon.pandarium_cluster.tasks.ConfirmUserTask;
import com.playtagon.pandarium_cluster.tasks.ConfirmUserTask.ConfirmUserResult;
import com.playtagon.pandarium_cluster.tasks.ConfirmUserTask.UserInfo;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;
import com.playtagon.pandarium_web.utils.LongOperation;

import lombok.Getter;

public class ConfirmationViewModel {
	@Getter
	private String error;

	@Init
	public void init(@ExecutionArgParam("token") String token) {
		error = "";
		if (token == null)
			return;

		LongOperation longOperation = new LongOperation() {

			@Override
			protected void execute() throws InterruptedException {
				HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
				IExecutorService executorService = hz.getExecutorService("default");
				Future<ConfirmUserResult> future = executorService.submit(new ConfirmUserTask(token));

				try {
					ConfirmUserResult result = future.get();
					result = future.get();
					activate();
					processResult(result);
					deactivate();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		};

		longOperation.start();
	}

	private void processResult(ConfirmUserResult result) {
		if (result == null || result.getUserInfo() == null) {
			error = "Sorry, but this link is broken or has already been used";
			BindUtils.postNotifyChange(null, null, this, "error");
			return;
		}

		UserInfo userInfo = result.getUserInfo();
		try {
			saveCookies(result.getUserInfo().getPrivateToken());
		} catch (Exception e) {
			e.printStackTrace();
		}
		Executions.sendRedirect("/#/progress/" + userInfo.getPrivateToken());

	}

	private void saveCookies(String privateToken) throws Exception {
		HttpServletResponse response = (HttpServletResponse) Executions.getCurrent().getNativeResponse();
		Cookie userCookie = new Cookie("user", privateToken);
		response.addCookie(userCookie);
	}
}
