package com.playtagon.pandarium_web.view_models;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IExecutorService;
import com.playtagon.pandarium_cluster.models.FriendInfo;
import com.playtagon.pandarium_cluster.models.UserProgress;
import com.playtagon.pandarium_cluster.tasks.GetUserProgressTask;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;

import lombok.Getter;

public class UserProgressViewModel {
	@Getter
	private UserProgress progress;

	@Getter
	private int maxFriendsListPageSize = 10;

	@Getter
	private int currentFriendsListPage = 0;

	@Getter
	private int totalFriendsListPage = 1;

	@Getter
	private int totalFriendsCount;

	@Getter
	private int activeFriendsCount;

	@Getter
	private List<FriendInfo> friendsInfo;

	@Init
	public void init(@ExecutionArgParam("token") String token) {
		final String privateToken = token;

		HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
		IExecutorService executor = hz.getExecutorService("default");
		Future<UserProgress> future = executor.submit(new GetUserProgressTask(privateToken));

		try {
			progress = future.get();
			processUserProgress();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	public String getFriendsRatio() {
		int ratio = 0;
		if (progress != null && totalFriendsCount != 0) {
			ratio = activeFriendsCount * 100 / totalFriendsCount;
		}
		return ratio + "%";
	}

	@Command
	@NotifyChange({ "friendsInfo", "currentFriendsListPage" })
	public void nextFriendsPage(@BindingParam("active") boolean active) {
		if (!active)
			return;
		System.out.println("nextFriendsPage");
		if (currentFriendsListPage >= totalFriendsListPage)
			return;

		currentFriendsListPage++;
		int fromIndex = maxFriendsListPageSize * currentFriendsListPage;
		int toIndex = Math.min(fromIndex + maxFriendsListPageSize, totalFriendsCount);
		friendsInfo = ((List<FriendInfo>) progress.getFriends()).subList(fromIndex, toIndex);
	}

	@Command
	@NotifyChange({ "friendsInfo", "currentFriendsListPage" })
	public void previousFriendsPage(@BindingParam("active") boolean active) {
		if (!active)
			return;
		System.out.println("previousFriendsPage");
		if (currentFriendsListPage < 1)
			return;

		currentFriendsListPage--;
		int fromIndex = maxFriendsListPageSize * currentFriendsListPage;
		friendsInfo = ((List<FriendInfo>) progress.getFriends()).subList(fromIndex, fromIndex + maxFriendsListPageSize);
	}

	private void processUserProgress() {
		if (progress == null) {
			Executions.sendRedirect("404.zul");
			return;
		}

		totalFriendsCount = progress.getFriends().size();
		activeFriendsCount = (int) progress.getActiveFriends();
		totalFriendsListPage = (int) Math.ceil(totalFriendsCount / 10.0);

		friendsInfo = progress.getFriends().subList(0, Math.min(maxFriendsListPageSize, totalFriendsCount));

		BindUtils.postNotifyChange(null, null, this, "progress");
		BindUtils.postNotifyChange(null, null, this, "friendsInfo");
		BindUtils.postNotifyChange(null, null, this, "totalFriendsCount");
		BindUtils.postNotifyChange(null, null, this, "activeFriendsCount");
		BindUtils.postNotifyChange(null, null, this, "friendsRatio");
	}

	private String getPrivateTokenFromCookies() {
		Cookie[] cookies = ((HttpServletRequest) Executions.getCurrent().getNativeRequest()).getCookies();

		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("user"))
				return cookie.getValue();
		}
		return null;
	}
}
