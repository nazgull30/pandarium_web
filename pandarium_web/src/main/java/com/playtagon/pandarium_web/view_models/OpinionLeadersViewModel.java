package com.playtagon.pandarium_web.view_models;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.util.Clients;

import com.hazelcast.core.HazelcastInstance;
import com.playtagon.pandarium_cluster.services.MailService;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;
import com.playtagon.pandarium_cluster.utils.Utils;
import com.playtagon.pandarium_web.utils.LongOperation;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OpinionLeadersViewModel {

	@Getter
	@Setter
	private String fullName;

	@Getter
	@Setter
	private String email;

	@Getter
	@Setter
	private String description;

	@Command
	public void save() {
		log.info("opinion email: " + email);
		log.info("opinion fullname: " + fullName);
		log.info("opinion desct: " + description);

		LongOperation operation = new LongOperation() {

			@Override
			protected void execute() throws InterruptedException {
				HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
				String baseUrl = Utils.getBaseUrl(hz);
				MailService mailService = new MailService(baseUrl);

				activate();
				addOpinionLeader(mailService);
				deactivate();
			}
		};
		operation.start();
	}

	private void addOpinionLeader(MailService mailService) {
		try {
			mailService.addOpinionLeader(email, fullName, description);
			Clients.evalJavaScript("showOpinionLeadersRegisterSuccess()");
			email = "";
		} catch (Exception e) {
			String error = "Email " + email + " has already been added to Opinion leaders";
			log.info(error);
			Clients.evalJavaScript("showOpinionLeadersEmailError('" + error + "')");
		}
		fullName = "";
		description = "";

		BindUtils.postNotifyChange(null, null, this, "message");
		BindUtils.postNotifyChange(null, null, this, "error");
		BindUtils.postNotifyChange(null, null, this, "email");
		BindUtils.postNotifyChange(null, null, this, "fullName");
		BindUtils.postNotifyChange(null, null, this, "description");
	}

}
