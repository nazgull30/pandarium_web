package com.playtagon.pandarium_web.adminpanel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IExecutorService;
import com.playtagon.pandarium_cluster.models.EmailData;
import com.playtagon.pandarium_cluster.tasks.CreatePrelaunchUserEntitiesFromLandingAndNotifyTask;
import com.playtagon.pandarium_cluster.tasks.GetUserEmailsTask;
import com.playtagon.pandarium_cluster.tasks.SendPresaleLetterTask;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;
import com.playtagon.pandarium_web.utils.LongOperation;

public class ApEmailViewModel {
	private Logger logger = LoggerFactory.getLogger(ApEmailViewModel.class);

	private Collection<String> emailsToDelivery = new ArrayList<>();

	private List<EmailData> emailDatas;

	private IExecutorService executor;

	@Init
	private void init() throws InterruptedException, ExecutionException {
		HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
		this.executor = hz.getExecutorService("default");

		Desktop desktop = Executions.getCurrent().getDesktop();
		desktop.enableServerPush(true);

		LongOperation operation = new LongOperation() {

			@Override
			protected void execute() throws InterruptedException {
				Future<List<EmailData>> future = executor.submit(new GetUserEmailsTask());
				try {
					List<EmailData> emailDatas = future.get();
					activate();
					processEmails(emailDatas);
					deactivate();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		operation.start();
	}

	public Collection<EmailData> getEmailDatas() {
		return emailDatas;
	}

	@Command
	public void changeEmailDistribution(@BindingParam("email") String email, @BindingParam("deliver") boolean deliver) {
		if (deliver) {
			emailsToDelivery.add(email);
		} else {
			emailsToDelivery.remove(email);
		}
		logger.info("changeEmailDistribution, emailsToDelivery size: " + emailsToDelivery);
	}

	@Command
	public void sendPresaleNotification() {
		if (emailsToDelivery.size() > 0) {
			executor.execute(new SendPresaleLetterTask(emailsToDelivery));
		}
	}

	@Command
	public void createPrelaunchUsersFromLandingAndNotify() {
		HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
		this.executor = hz.getExecutorService("default");
		executor.execute(new CreatePrelaunchUserEntitiesFromLandingAndNotifyTask(0, true));
	}

	private void processEmails(List<EmailData> emailDatas) {
		this.emailDatas = emailDatas;
		BindUtils.postNotifyChange(null, null, this, "emailDatas");
	}

}
