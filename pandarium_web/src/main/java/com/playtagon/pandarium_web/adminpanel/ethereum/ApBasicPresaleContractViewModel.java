package com.playtagon.pandarium_web.adminpanel.ethereum;

import org.web3j.crypto.WalletFile;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;

import com.hazelcast.core.HazelcastInstance;
import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;

import lombok.Getter;
import lombok.Setter;

public abstract class ApBasicPresaleContractViewModel {
	@Getter
	protected WalletFile ceoWallet;
	
	@Getter
	protected String presaleContractAddress;

	@Getter
	protected String ethereumNode;

	@Getter
	protected String error;
	
	@Setter
	protected int gasPrice;
	@Setter
	protected int gasLimit;
	

	protected CommonSettingsDao settingsDao;

	@Init
	public void init(@ExecutionArgParam("ceoWallet") WalletFile ceoWallet,
			@ExecutionArgParam("contractAddress") String presaleContractAddress,
			@ExecutionArgParam("ethereumNode") String ethereumNode) {
		HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
		this.settingsDao = new CommonSettingsDao(hz);
		this.ceoWallet = ceoWallet;
		this.presaleContractAddress = presaleContractAddress;
		this.ethereumNode = ethereumNode;

	}
	
}
