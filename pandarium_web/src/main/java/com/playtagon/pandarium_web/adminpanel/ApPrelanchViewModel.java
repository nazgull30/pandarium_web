package com.playtagon.pandarium_web.adminpanel;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.crypto.WalletFile;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.http.HttpService;
import org.web3j.utils.Convert;
import org.web3j.utils.Convert.Unit;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Window;

import com.hazelcast.core.HazelcastInstance;
import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.models.PrelaunchRewardParams;
import com.playtagon.pandarium_cluster.services.AdminService;
import com.playtagon.pandarium_cluster.services.PrelaunchService;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;
import com.playtagon.pandarium_entities.CommonSettings;

public class ApPrelanchViewModel {
	private Logger logger = LoggerFactory.getLogger(ApPrelanchViewModel.class);

	private Collection<CommonSettings> settings;

	private float presaleContractBalance;
	private float ceoBalance;

	private WalletFile ceoWallet;

	private long registeredUserTotalCount;
	private long promoPandasForUsersTotalCount;

	private PrelaunchRewardParams prelaunchRewardParams;

	private PrelaunchService prelaunchService;
	private AdminService adminService;

	private CommonSettingsDao settingsDao;

	private String error;

	private Web3j web3j;

	private boolean contractStarted;

	@Init
	public void init() {
		HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
		settingsDao = new CommonSettingsDao(hz);
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		prelaunchService = new PrelaunchService(userDao, settingsDao);
		adminService = new AdminService(settingsDao);

		prelaunchRewardParams = prelaunchService.getRewardParams();
		if (prelaunchRewardParams == null) {
			prelaunchRewardParams = new PrelaunchRewardParams();
		}

		logger.info("friendCountThreshold: " + prelaunchRewardParams.getFriendCountThreshold());

		registeredUserTotalCount = userDao.count();
		promoPandasForUsersTotalCount = prelaunchService.calculateTotalPandaPromo(prelaunchRewardParams);

		settings = settingsDao.getMap().values();

		ceoWallet = adminService.loadCeoWallet();

		initEthereum();
	}

	public Web3j getWeb3j() {
		return web3j;
	}

	public WalletFile getCeoWallet() {
		return ceoWallet;
	}

	public float getPresaleContractBalance() {
		return presaleContractBalance;
	}

	public float getCeoBalance() {
		return ceoBalance;
	}

	@DependsOn("web3j")
	public boolean isEthereumConnectionEstablished() {
		return web3j != null;
	}

	public boolean isContractStarted() {
		return contractStarted;
	}

	public String getError() {
		return error;
	}

	public Collection<CommonSettings> getSettings() {
		return settings;
	}

	public long getRegisteredUserTotalCount() {
		return registeredUserTotalCount;
	}

	public long getPromoPandasForUsersTotalCount() {
		return promoPandasForUsersTotalCount;
	}

	public PrelaunchRewardParams getPrelaunchRewardParams() {
		return prelaunchRewardParams;
	}

	public void setPrelaunchRewardParams(PrelaunchRewardParams prelaunchRewardParams) {
		this.prelaunchRewardParams = prelaunchRewardParams;
	}

	@Command
	@NotifyChange("prelaunchRewardParams")
	public void savePelaunchRewardParameters() {
		adminService.savePelaunchRewardParameters(prelaunchRewardParams);
		logger.info("saveOrUpdatePelaunchRewardParameters");
	}

	@Command
	@NotifyChange("settings")
	public void createSettings(@BindingParam("key") String key, @BindingParam("val") String value) {
		settings = adminService.createOrUpdateSettings(key, value);
	}

	@Command
	@NotifyChange({ "error", "ceoWallet", "settings" })
	public void uploadCeoWallet(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
		error = "";
		UploadEvent event = (UploadEvent) ctx.getTriggerEvent();
		Media media = event.getMedia();
		byte[] walletBytes = media.getByteData();
		ceoWallet = adminService.trySaveCeoWallet(walletBytes);
		if (ceoWallet == null) {
			error = Labels.getLabel("wrong_wallet_format");
		} else {
			settings = settingsDao.getMap().values();
		}
	}

	@Command
	@NotifyChange("error")
	public void openStartContractForm() {
		error = "";
		CommonSettings contractAddress = settingsDao.getById("presaleContractAddress");
		if (contractAddress == null) {
			error = Labels.getLabel("no_contract_address");
			return;
		}

		if (ceoWallet == null) {
			error = Labels.getLabel("no_ceo_wallet");
			return;
		}

		CommonSettings ethereumNode = settingsDao.getById("ethereumNode");
		if (ethereumNode == null) {
			error = Labels.getLabel("ethereum_connection_error");
			return;
		}

		Map<String, Object> arg = new HashMap<>();
		arg.put("ceoWallet", ceoWallet);
		arg.put("contractAddress", contractAddress.getValue());
		arg.put("ethereumNode", ethereumNode.getValue());

		Window window = (Window) Executions.createComponents("/adminpanel/adminpanel_presale_contract_start.zul", null,
				arg);
		window.setWidth("500px");
		window.setHeight("500px");
		window.doModal();
	}

	private void initEthereum() {
		web3j = Web3j.build(new HttpService());

		CommonSettings contractAddress = settingsDao.getById("presaleContractAddress");
		if (contractAddress == null)
			return;

		try {
			EthGetBalance contractBalanceEth = web3j
					.ethGetBalance(contractAddress.getValue(), DefaultBlockParameterName.LATEST).sendAsync().get();
			presaleContractBalance = Convert.fromWei(new BigDecimal(contractBalanceEth.getBalance()), Unit.ETHER)
					.floatValue();
		} catch (InterruptedException | ExecutionException e) {
			// e.printStackTrace();
		}

		if (ceoWallet == null)
			return;

		try {
			EthGetBalance ceoBalanceEth = web3j
					.ethGetBalance("0x" + ceoWallet.getAddress(), DefaultBlockParameterName.LATEST).sendAsync().get();
			ceoBalance = Convert.fromWei(new BigDecimal(ceoBalanceEth.getBalance()), Unit.ETHER).floatValue();

		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}
}
