package com.playtagon.pandarium_web.adminpanel.ethereum;

import java.io.IOException;
import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.Wallet;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.infura.InfuraHttpService;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.EventListener;

import com.playtagon.pandarium_cluster.contracts.PresaleCampaign;
import com.playtagon.pandarium_entities.CommonSettings;

public class ApStartContractViewModel extends ApBasicPresaleContractViewModel
		implements EventListener<StartContractResponseEvent> {
	private Logger logger = LoggerFactory.getLogger(ApStartContractViewModel.class);

	private int totalPandas;
	private int durationInMinutes;
	private double pandaPriceInETH;

	@Init(superclass = true)
	public void init() {
		Executions.getCurrent().getDesktop().enableServerPush(true);
	}

	public Validator getValidator() {
		return new AbstractValidator() {

			@Override
			public void validate(ValidationContext ctx) {
				if (ceoWallet == null) {
					addInvalidMessage(ctx, Labels.getLabel("no_ceo_wallet"));
					return;
				}

				if (ethereumNode == null) {
					addInvalidMessage(ctx, "ethereumNode null");
					return;
				}

				CommonSettings presaleContractAddress = settingsDao.getById("presaleContractAddress");
				if (presaleContractAddress.getValue() == null) {
					addInvalidMessage(ctx, Labels.getLabel("no_presale_contract_address"));
					return;
				}
			}
		};
	}

	public void setDurationInMinutes(int durationInMinutes) {
		this.durationInMinutes = durationInMinutes;
	}

	public void setPandaPriceInETH(double pandaPriceInETH) {
		this.pandaPriceInETH = pandaPriceInETH;
	}

	public void setTotalPandas(int totalPandas) {
		this.totalPandas = totalPandas;
	}

	@Command
	public void startPresale(@BindingParam("password") String password) {
		try {
			Credentials credentials = Credentials.create(Wallet.decrypt(password, ceoWallet));

			logger.info("presaleContractAddress: " + presaleContractAddress);
			logger.info("ethereumNode: " + ethereumNode);
			Web3j web3j = Web3j.build(new InfuraHttpService(ethereumNode));
			PresaleCampaign presaleCampaign = PresaleCampaign.load(presaleContractAddress, web3j, credentials,
					BigInteger.valueOf(gasPrice), BigInteger.valueOf(gasLimit));

			logger.info("contract is valid: " + (presaleCampaign.isValid()));

			// ExecutorService executor = ExecutorHolder.getExecutor();
			// executor.execute(new
			// StartContractRunnable(Executions.getCurrent().getDesktop(), this,
			// presaleCampaign,
			// totalPandas, durationInMinutes, pandaPriceInETH));

		} catch (CipherException | IOException e) {
			System.out.println("Typed pass: " + password + ", wallet addr: " + ceoWallet.getAddress());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onEvent(StartContractResponseEvent event) throws Exception {
		logger.info("start contract over");
	}

}
