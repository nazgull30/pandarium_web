package com.playtagon.pandarium_web.adminpanel;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;

public class ApLoginViewModel {
	private Logger logger = LoggerFactory.getLogger(ApLoginViewModel.class);

	private String username;

	private String password;

	private String error;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Command
	@NotifyChange("error")
	public void login() {
		System.out.println("LOGIN");
		error = "";
		AuthenticationToken token = new UsernamePasswordToken(username, password);
		try {
			SecurityUtils.getSubject().login(token);
			logger.info("credentials is right");
			if (SecurityUtils.getSubject().isAuthenticated()) {
				Executions.sendRedirect("adminpanel.zul");
			}

		} catch (UnknownAccountException e) {
			error = "Account does not exists";
			logger.warn(error);
		} catch (IncorrectCredentialsException e) {
			error = "Submitted credentials for username " + username + " did not match the expected credentials";
			logger.warn(error);
		}

	}
}
