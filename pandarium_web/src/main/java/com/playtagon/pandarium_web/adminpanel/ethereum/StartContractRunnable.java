package com.playtagon.pandarium_web.adminpanel.ethereum;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.utils.Convert;
import org.web3j.utils.Convert.Unit;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.EventListener;

import com.playtagon.pandarium_cluster.contracts.PresaleCampaign;

public class StartContractRunnable implements Runnable {
	private Logger logger = LoggerFactory.getLogger(StartContractRunnable.class);

	private Desktop desktop;
	private EventListener<StartContractResponseEvent> listener;
	private PresaleCampaign presaleCampaign;

	private int totalPandas;
	private int durationInMinutes;
	private double pandaPriceInETH;

	public StartContractRunnable(Desktop desktop, EventListener<StartContractResponseEvent> listener,
			PresaleCampaign presaleCampaign, int totalPandas, int durationInMinutes, double pandaPriceInETH) {
		super();
		this.desktop = desktop;
		this.listener = listener;
		this.presaleCampaign = presaleCampaign;
		this.totalPandas = totalPandas;
		this.durationInMinutes = durationInMinutes;
		this.pandaPriceInETH = pandaPriceInETH;
	}

	@Override
	public void run() {
		try {
			presaleCampaign.start(BigInteger.valueOf(totalPandas), BigInteger.valueOf(durationInMinutes),
					Convert.toWei(BigDecimal.valueOf(pandaPriceInETH), Unit.ETHER).toBigInteger()).send();

			Executions.schedule(desktop, listener, new StartContractResponseEvent());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
