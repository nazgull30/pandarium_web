package com.playtagon.pandarium_web.security;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SaltedAuthenticationInfo;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import com.hazelcast.core.HazelcastInstance;
import com.playtagon.pandarium_cluster.dao.AdminDao;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;
import com.playtagon.pandarium_entities.Admin;

public class AdminRealm extends AuthorizingRealm {

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
		Set<String> roles = new HashSet<>();
		roles.add("administrator");
		authorizationInfo.setRoles(roles);
		return authorizationInfo;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		UsernamePasswordToken upToken = (UsernamePasswordToken) token;

		HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
		AdminDao adminDao = new AdminDao(hz);

		Admin admin = adminDao.findAdminByUsername(upToken.getUsername());

		if (admin == null) {
			throw new UnknownAccountException();
		}

		SaltedAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(admin.getUsername(),
				admin.getHashedPassword(), ByteSource.Util.bytes(admin.getPasswordSalt()), "userName");
		return authenticationInfo;
	}

}
