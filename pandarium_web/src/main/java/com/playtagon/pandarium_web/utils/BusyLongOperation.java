package com.playtagon.pandarium_web.utils;

public abstract class BusyLongOperation extends LongOperation {

	protected void showBusy(String busyMessage) throws InterruptedException {
		activate();
		deactivate();
	}

	@Override
	protected void onCleanup() {
	}
}