package com.playtagon.pandarium_web.checkers;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.projection.Projection;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.Predicates;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;
import com.playtagon.pandarium_entities.PrelaunchUser;

@SuppressWarnings("serial")
public class EmailAndIpChecker implements Serializable {
	private Logger logger = LoggerFactory.getLogger(EmailAndIpChecker.class);

	/**
	 * Checks if email exists.
	 *
	 * @param email
	 *            email to validate.
	 * @return
	 */
	public boolean emailExists(String email) {
		HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		return userDao.checkIfUserWithEmailExists(email);
	}

	/**
	 * System checks if email have been registered from current ip address. Only up
	 * to 2 emails are allowed.
	 * 
	 * @param ip
	 *            ip to validate
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean has2RegisteredEmailsFromIp(String ip) {
		HazelcastInstance hz = HazelcastUtil.getInstance().getHazelcastClient();
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		Predicate predicate = Predicates.equal("prelaunchRegistration.ip", ip);
		Collection<String> emailsFromIp = userDao.getMap()
				.project(new Projection<Map.Entry<ObjectId, PrelaunchUser>, String>() {

					@Override
					public String transform(Entry<ObjectId, PrelaunchUser> input) {
						return input.getValue().getPrelaunchRegistration().getIp();
					}
				}, predicate);
		return emailsFromIp.size() >= 2;
	}

}
