package com.playtagon.pandarium_web.routing;

import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.util.*;
import java.util.*;

public class Router 
{
	private Map<String, Route> routesWithoutParams = new TreeMap<String, Route>();
	private Map<String, Route> routesWithParams = new TreeMap<String, Route>();
	private Component contentHolder = null;
	private Component content = null;

	public Router(Map<String, Route> routesWithoutParams, Map<String, Route> routesWithParams) 
	{
		this.routesWithoutParams = routesWithoutParams;
		this.routesWithParams = routesWithParams;
	}

	private String removeFirstAndLastSlash(String url) 
	{
		url = url.replaceAll("^/", "");
		url = url.replaceAll("/$", "");
		return url;
	}

	public void setContentHolder(Component contentHolder) 
	{
		this.contentHolder = contentHolder;
	}

	public Component getContentHolder() 
	{
		return contentHolder;
	}

	public void dispatch(String url) 
	{
		url = removeFirstAndLastSlash(url);
		
		Route route = findRoute(url);
		if (route == null) {
			content = Executions.createComponents("/404.zul", contentHolder, null);
			return;
		} else {			
			Map<String, Object> pathVariables = new HashMap<String, Object>();
			try {
				pathVariables = route.resolvePathVariables(url);
			} catch (RuntimeException e) {
				return;
			}
			
			if (content != null) {
				content.detach();
				content = null;
			}
			detachOldComponents();
			content = Executions.createComponents(route.getView(), contentHolder, pathVariables);
		}
	}
	
	private void detachOldComponents() {
		Component parent = Path.getComponent("/layoutWindow/contentHolder");			
		List<Component> children = parent.getChildren();
		for (int i = 0; i < children.size(); i ++) {				
			children.get(i).detach();
		}
	}

	private Route findRoute(String url) 
	{
		for (String testUrl : routesWithoutParams.keySet()) {
			Route route = routesWithoutParams.get(testUrl);
			if (route.matches(url)) {
				return route;
			}
		}

		for (String testUrl : routesWithParams.keySet()) {
			Route route = routesWithParams.get(testUrl);
			if (route.matches(url)) {
				return route;
			}
		}

		return null;
	}
	
	public void goTo(String url) 
	{
		url = url.replaceAll("#", "");
		Clients.evalJavaScript("window.location.hash = '#" + url + "';");
	}
	
	public static Router getCurrent() 
	{
		Execution execution = Executions.getCurrent();
		if(execution != null && execution.getDesktop() != null) {
			Object router = execution.getDesktop().getAttribute("router");
			if(router != null && router instanceof Router) {
				return (Router) router;
			}
		}
		return null;
	}
}
