package com.playtagon.pandarium_web.routing;

import org.zkoss.zk.ui.*;
import java.util.*;

public class RouterFactory 
{
	private Map<String, Route> routesWithoutParams = new TreeMap<String, Route>();
	private Map<String, Route> routesWithParams = new TreeMap<String, Route>();
	
	public Route addRoute(String url, String view)
	{
		url = url.toLowerCase();
		url = removeFirstAndLastSlash(url);
		Route route = new Route(url, view);
		if (url.contains("{")) {
			routesWithParams.put(url, route);
		} else {
			routesWithoutParams.put(url, route);
		}
		return route;
	}

	private String removeFirstAndLastSlash(String url) 
	{
		url = url.replaceAll("^/", "");
		url = url.replaceAll("/$", "");
		return url;
	}
	
	public Router createRouter(Component contentHolder) 
	{
		Router router = new Router(routesWithoutParams, routesWithParams);
		router.setContentHolder(contentHolder);
		Executions.getCurrent().getDesktop().setAttribute("router", router);
		return router;
	}
	
	public boolean routeVaid(String route)
	{
		return routesWithoutParams.containsKey(route) || routesWithParams.containsKey(route);
	}
}