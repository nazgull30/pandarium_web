import Pages.ProgressPage;
import Pages.RegistrationPage;
import Utils.Cluster;
import Utils.DataBase;
import com.codeborne.selenide.Configuration;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Title;

import java.awt.*;

import static com.codeborne.selenide.WebDriverRunner.clearBrowserCache;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;


public class PrelaunchTest {

    //Not registered user
    private final String userNotReg = "pandarium.test@gmail.com";
    //Already registered user
    private final String userReg = "pandarium.test2@gmail.com";
    //User for invite tests
    private final String userForInvite = "pandarium.test3@gmail.com";
    
    private String ENV = System.getProperty("env");
    private String BASE_URL = System.getProperty("url");

    //private String ENV = "dev";
    //private String BASE_URL = "http://dev.pandarium.co/#/";

    private String BROWSER = System.getProperty("browser");
    private DataBase dataBase = new DataBase(ENV);
    private Cluster cluster;

    @BeforeClass
    public void setUp() {
        if(BROWSER==null){
            Configuration.browser = "Chrome";
        }
        else if(BROWSER.equalsIgnoreCase("Firefox")){
            Configuration.browser = "Firefox";
        }
        else if(BROWSER.equalsIgnoreCase("Safari")){
            Configuration.browser = "Safari";
        }
        else if(BROWSER.equalsIgnoreCase("Chrome")){
            Configuration.browser = "Chrome";
        }
        else if(BROWSER.equalsIgnoreCase("Opera")) {
            Configuration.browser = "Opera";
        }

        if (BASE_URL==(null)) {
            Configuration.baseUrl = "http://localhost:8080/#/";
        } else {
            Configuration.baseUrl = BASE_URL;
        }
        System.out.println(BASE_URL);
        System.out.println(ENV);
        cluster = new Cluster(ENV);
        clearBrowserCache();
        Configuration.timeout = 15000;
        Configuration.startMaximized = true;
        System.out.println("Before class");
    }

    @AfterClass
    public void tearDown(){
        System.out.println("After class");
    }

    @BeforeMethod
    public void checkThatUserNotRegistered(){

        if (dataBase.isUserRegistred(userNotReg)){
            cluster.deleteUserByEmail(userNotReg);
        }
        if (dataBase.isUserRegistred(userForInvite)){
            cluster.deleteUserByEmail(userForInvite);
        }
        if (!dataBase.isUserRegistred(userReg)){
            registerUser(userReg);
        }
        System.out.println("Before method");
    }

    @AfterMethod()
    public void afterTestGroupAll(){
        getWebDriver().quit();
        System.out.println("After method");
    }

    @Title("Prelaunch user registration test")
    @Test(groups = {"All","registration"})
    public void prelaunchUserRegistrationTest() {
        RegistrationPage registrationPage = new RegistrationPage(ENV);
        registrationPage.openRegistrationPage();
        registrationPage.enterEmail(userNotReg);
        registrationPage.clickPandoptButton();
        registrationPage.isEmailVerificationMessageIsVisible();
        registrationPage.checkActiveStatusIsFalse(userNotReg);
        registrationPage.confirmEmail(userNotReg);
        registrationPage.checkActiveStatusIsTrue(userNotReg);

        ProgressPage progressPage = new ProgressPage(ENV);
        progressPage.isProgressPageOpened();
    }

    @Title("Check that user can't register one email twice")
    @Test(groups = {"All","registration"}, priority = 1)
    public void registrationWithAlreadyRegisteredEmail(){
        RegistrationPage registrationPage = new RegistrationPage(ENV);
        registrationPage.openRegistrationPage();
        registrationPage.enterEmail(userNotReg);
        registrationPage.clickPandoptButton();
        registrationPage.isEmailVerificationMessageIsVisible();

        registrationPage.openRegistrationPage();
        registrationPage.enterEmail(userNotReg);
        registrationPage.clickPandoptButton();
        registrationPage.isEmailAlreadyRegistredMessageIsVisible();
    }

    @DataProvider
    public Object[] invalidEmails() {
        return new Object[]{"pandarium.testgmail.com", "pandarium.test@gmailcom",
                "@pandarium.test.gmailcom", "pandariu!:m.test@gmailcom"};
    }

    @Title("Registration email validation test")
    @Test(dataProvider = "invalidEmails")
    public void registrationEmailValidationTest(String email) {
        RegistrationPage registrationPage = new RegistrationPage(ENV);

        registrationPage.openRegistrationPage();
        registrationPage.enterEmail(email);
        registrationPage.clickPandoptButton();
        registrationPage.invalidEmailMessageIsVisisble();
    }

    @Title("Check layout registration page")
    @Test(groups = "All")
    public void checkLayoutRegistrationPage(){
        RegistrationPage registrationPage = new RegistrationPage(ENV);

        registrationPage.openRegistrationPage();
        registrationPage.checkVisibilityElements();
    }

    @Title("Check layout progress page")
    @Test(groups = "All", priority = 1)
    public void checkLayoutProgressPge(){
        ProgressPage progressPage = new ProgressPage(ENV);

        progressPage.openProgressPage(userReg);
        progressPage.checkVisibilityElements();
    }

    @Title("Check layout terms")
    @Test(groups = "All", priority = 1)
    public void checkLayoutTherms(){
        ProgressPage progressPage = new ProgressPage(ENV);

        progressPage.openProgressPage(userReg);
        progressPage.clickThermsButton();
        progressPage.checkVisibilityThermsElements();
        progressPage.clickThermsCloseButton();
        progressPage.isProgressPageOpened();
    }

    @Title("Check layout Opinion Leaders")
    @Test(groups = "All", priority = 1)
    public void checkLayoutOpinionLeaders(){
        ProgressPage progressPage = new ProgressPage(ENV);

        progressPage.openProgressPage(userReg);
        progressPage.clickOpinionLeadersButton();
        progressPage.checkVisibilityOpinionElements();
        progressPage.clickCloseOpinionLeaderButton();
        progressPage.isProgressPageOpened();
    }

    @Title("Opinion leaders email validation test")
    @Test(dataProvider = "invalidEmails", priority = 1)
    public void opinionEmailValidationTest(String email){
        ProgressPage progressPage = new ProgressPage(ENV);

        progressPage.openProgressPage(userReg);
        progressPage.clickOpinionLeadersButton();
        progressPage.enterName();
        progressPage.enterEmail(email);
        progressPage.enterDescription();
        progressPage.clickSendButton();
        progressPage.checkEnterRealEmailErorrIsVisible();
    }

    @Title("Full name is required")
    @Test(groups = "All", priority = 1)
    public void fullNameIsRequired(){
        ProgressPage progressPage = new ProgressPage(ENV);

        progressPage.openProgressPage(userReg);
        progressPage.clickOpinionLeadersButton();
        progressPage.enterEmail(userReg);
        progressPage.enterDescription();
        progressPage.clickSendButton();
        progressPage.fieldIsRequiredErrorIsVisible();
    }

    @Title("Description is required")
    @Test(groups = "All", priority = 1)
    public void descriptionIsRequired(){
        ProgressPage progressPage = new ProgressPage(ENV);

        progressPage.openProgressPage(userReg);
        progressPage.clickOpinionLeadersButton();
        progressPage.enterName();
        progressPage.enterEmail(userReg);
        progressPage.clickSendButton();
        progressPage.fieldIsRequiredErrorIsVisible();
    }

    @Title("Send opinion leader")
    @Test(groups = "All", priority = 1)
    public void sendOpinionLeader(){
        ProgressPage progressPage = new ProgressPage(ENV);

        progressPage.openProgressPage(userReg);
        progressPage.clickOpinionLeadersButton();
        progressPage.enterName();
        progressPage.enterEmail(userReg);
        progressPage.enterDescription();
        progressPage.clickSendButton();
        progressPage.isProgressPageOpened();
    }

    @Title("Check support chat visibility")
    @Test(groups = "All")
    public void checkSupportChat(){
        RegistrationPage registrationPage = new RegistrationPage(ENV);

        registrationPage.openRegistrationPage();
        registrationPage.checkChatVisibility();
    }

    @Title("Copy url button test")
    @Test(groups = "All", priority = 1)
    public void copyUrlButtonTest() throws AWTException {
        ProgressPage progressPage = new ProgressPage(ENV);

        progressPage.openProgressPage(userReg);
        progressPage.clickCopyUrlButton();
        progressPage.openPageFromClipboard();

        RegistrationPage registrationPage = new RegistrationPage(ENV);
        registrationPage.switchToRegistrationPage();
        registrationPage.isInvitedByMessageIsVisible();
    }

    //invite counts active, invite status
    @Title("Accepted invitation counter test")
    @Test(groups = "All", priority = 1)
    public void acceptedInvitationTest() throws AWTException {
        ProgressPage progressPage = new ProgressPage(ENV);

        progressPage.openProgressPage(userReg);
        progressPage.clickCopyUrlButton();
        progressPage.openPageFromClipboard();

        RegistrationPage registrationPage = new RegistrationPage(ENV);
        registrationPage.switchToRegistrationPage();
        registrationPage.enterEmail(userForInvite);
        registrationPage.clickPandoptButton();
        registrationPage.isEmailVerificationMessageIsVisible();
        registrationPage.confirmEmail(userForInvite);
        getWebDriver().close();

        progressPage.openProgressPage(userReg);
        progressPage.checkCounter();
        progressPage.checkUserHaveReferralEmailInDB(userForInvite,userReg);
    }

    @Title("Google analytics script")
    @Test
    public void googleAnalyticsTest(){
        RegistrationPage registrationPage = new RegistrationPage(ENV);

        registrationPage.openRegistrationPage();
        registrationPage.checkGoogleAnalytics();
    }

    @Title("Dropbox test")
    @Test
    public void dropBoxTest(){
        RegistrationPage registrationPage = new RegistrationPage(ENV);

        registrationPage.openRegistrationPage();
        registrationPage.openEnglishBambooPaper();
        registrationPage.isBambooPaperOpened();
    }

    @Title("Discord button test")
    @Test
    public void discordButtonTest(){
        RegistrationPage registrationPage = new RegistrationPage(ENV);

        registrationPage.openRegistrationPage();
        registrationPage.clickDiscordButton();
        registrationPage.isDiscordPageOpened();
    }

    public void registerUser(String user){
        RegistrationPage registrationPage = new RegistrationPage(ENV);

        registrationPage.openRegistrationPage();
        registrationPage.enterEmail(user);
        registrationPage.clickPandoptButton();
        registrationPage.isEmailVerificationMessageIsVisible();
        registrationPage.checkActiveStatusIsFalse(user);
        registrationPage.confirmEmail(user);
        registrationPage.checkActiveStatusIsTrue(user);
    }
}
