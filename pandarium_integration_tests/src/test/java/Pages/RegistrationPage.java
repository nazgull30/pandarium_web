package Pages;
import Utils.DataBase;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.Assert;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class RegistrationPage {

    private final String URL = baseUrl;

    private SelenideElement emailInput = $(By.id("emailInput"));
    private SelenideElement pandoptButton = $(By.id("pandopt"));
    private SelenideElement invalidEmailError = $(By.xpath("//div[contains(text(), 'Enter your real Email')]"));
    //message after click pandopt with valid email
    private SelenideElement emailVerificationMessage = $(By.id("emailVerification"));
    //message after input already registered email
    private SelenideElement emailAlreadyRegistredMessage = $(By.xpath("//div[contains(text(), 'This email has already been registered')]"));
    //logo in header
    private SelenideElement pandariumLogo = $(By.cssSelector("a.navbar-brand"));
    private SelenideElement emailRegistrationBlock = $(By.id("emailRegistration"));
    //main image
    private SelenideElement prelaunchPandaImage = $(By.id("prelaunchPanda"));
    private SelenideElement supportChat = $(By.xpath("//img[starts-with(@src, 'https://avatars.gosquared.com')]"));
    private SelenideElement invitedByMessage = $(By.id("invitedBy"));
    private SelenideElement pandaAttributes = $(By.cssSelector("div.panda-attributes"));
    private SelenideElement subscribersCounter = $(By.cssSelector("div.subscribers"));
    private SelenideElement roadMap = $(By.cssSelector("div.road-map"));
    private SelenideElement discordButton = $(By.cssSelector("a.discord-widget.ml-3"));
    private SelenideElement bambooPaperButton = $(By.id("dropdown-paper"));
    private SelenideElement englishLanguage = $(By.cssSelector("a.dropdown-item.britain-flag"));
    private DataBase dataBase;
    public RegistrationPage(String env){
         dataBase = new DataBase(env);
    }


    @Step("Open registration page")
    public void openRegistrationPage(){
        open(URL);
        pandoptButton.shouldBe(visible);
    }

    @Step("Enter email")
    public void enterEmail(String email){
        emailInput.click();
        emailInput.sendKeys(email);
    }

    @Step("click 'Pandopt' button")
    public void clickPandoptButton(){
        pandoptButton.click();
    }

    @Step("check that error is displayed")
    public void invalidEmailMessageIsVisisble(){
        Assert.assertTrue("Error message is missing",invalidEmailError.isDisplayed());
    }

    @Step("check that email verification message is visible")
    public void isEmailVerificationMessageIsVisible(){
        emailVerificationMessage.shouldBe(visible);
    }

    @Step("check that status in data base is 'false'")
    public void checkActiveStatusIsFalse(String email) {
        Assert.assertFalse(dataBase.getStatusActive(email));
    }

    @Step("confirm email")
    public void confirmEmail(String email) {
        String token = dataBase.getConfirmTokenByEmail(email);
        open(URL + "confirm/" + token);
    }

    @Step("check that status in data base is 'true'")
    public void checkActiveStatusIsTrue(String email) {
        Assert.assertTrue(dataBase.getStatusActive(email));
    }

    @Step("Check 'email already registred' error is visible")
    public void isEmailAlreadyRegistredMessageIsVisible() {
        emailAlreadyRegistredMessage.shouldBe(visible);
    }

    @Step("Check visibility elements registration page")
    public void checkVisibilityElements() {
        emailInput.shouldBe(visible);
        pandoptButton.shouldBe(visible);
        prelaunchPandaImage.shouldBe(visible);
        pandariumLogo.shouldBe(visible);
        emailRegistrationBlock.shouldBe(visible);
        pandaAttributes.shouldBe(visible);
        subscribersCounter.shouldBe(visible);
        roadMap.shouldBe(visible);
        discordButton.shouldBe(visible);
    }

    @Step("Check chat is visible")
    public void checkChatVisibility() {
        supportChat.shouldBe(visible);
    }

    @Step("Check 'invited by' message is visible")
    public void isInvitedByMessageIsVisible() {
        invitedByMessage.shouldBe(visible);
    }

    public void switchToRegistrationPage() {
        while (!emailInput.exists()) {
            for (String handle : getWebDriver().getWindowHandles()) { getWebDriver().switchTo().window(handle);}
        }
    }

    public void checkGoogleAnalytics() {
        List<SelenideElement> scriptList = $$(By.tagName("script"));
        boolean scriptFound = false;
        for(SelenideElement item : scriptList){
            scriptFound = item.getAttribute("src").contains("google-analytics.com");
            if (scriptFound){
                break;
            }
        }
        Assert.assertTrue("Google analytics script not found",scriptFound);
    }

    public void openEnglishBambooPaper() {
        bambooPaperButton.click();
        englishLanguage.click();
    }

    public void isBambooPaperOpened() {
        Selenide.switchTo().window("Bamboo paper - Pandarium.pdf");
        Assert.assertEquals("Bamboo paper - Pandarium.pdf",Selenide.title());
    }

    public void clickDiscordButton() {
        discordButton.click();
    }

    public void isDiscordPageOpened() {
        Selenide.switchTo().window("Discord");
        Assert.assertEquals("Discord",Selenide.title());
    }
}
