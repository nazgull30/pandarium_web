package Pages;

import Utils.DataBase;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

import java.awt.*;
import java.awt.event.KeyEvent;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
public class ProgressPage {

    private final String URL = baseUrl + "progress/";
    private final String NAME = "Panda Pandarium";

    private final String DESCRIPTION = "I'm hungry panda";

    private SelenideElement pandariumLogo = $(By.cssSelector("a.navbar-brand"));
    private SelenideElement pandariumLogoTerms = $(By.xpath("//div[@class = 'navbar-brand' and first()]"));
    private SelenideElement socialNetworks = $(By.cssSelector("div.jssocials-shares"));
    private SelenideElement referralLink = $(By.id("referralLink"));
    private SelenideElement copyUrlButton = $(By.id("copyUrl"));
    private SelenideElement progressBar = $(By.id("progressBar"));
    private SelenideElement termsButton = $(By.id("terms"));
    private SelenideElement opinionLeadersButton = $(By.id("opinionLeaders"));
    private SelenideElement termsTitle = $(By.id("termsTitle"));
    private SelenideElement termsText = $(By.id("termsText"));
    private SelenideElement termsCloseButton = $(By.id("termsClose"));
    private SelenideElement opinionLeadersTitle = $(By.id("opinionLeadersTitle"));
    private SelenideElement opinionLeadersText = $(By.id("opinionLeadersText"));
    private SelenideElement leaderNameField = $(By.id("leaderName"));
    private SelenideElement leaderEmailField = $(By.id("leaderEmail"));
    private SelenideElement leaderDescrField = $(By.id("leaderDescr"));
    private SelenideElement sendOpinionButton = $(By.id("saveOpinionLeaderBtn"));
    private SelenideElement closeOpinionLeaderButton = $(By.cssSelector("div.modal-full_close"));
    private SelenideElement enterYourRealEmailError = $(By.xpath("//div[contains(text(), 'Enter your real Email')]"));
    private SelenideElement fieldIsRequiredError = $(By.xpath("//div[contains(text(), 'The field is required')]"));
    private SelenideElement urlCopiedMessage = $(By.xpath("//div[contains(text(), 'URL Copied!')]"));
    private SelenideElement inviteCountActive = $(By.cssSelector("span.invite-count_active > span"));
    private SelenideElement inviteStatusActive = $(By.xpath("//div[@class = 'invite-status invite-status__active']"));
    private DataBase dataBase;

    public ProgressPage(String env){
        dataBase= new DataBase(env);
    }

    @Step("Check that progress page is opened")
    public void isProgressPageOpened(){
        referralLink.shouldBe(visible);
        copyUrlButton.shouldBe(visible);
    }

    @Step("Open progress page")
    public void openProgressPage(String email) {
        open(URL + dataBase.getPrivateTokenByEmail(email));
    }

    @Step("check visibility elements progress page")
    public void checkVisibilityElements() {
        pandariumLogo.shouldBe(visible);
        socialNetworks.shouldBe(visible);
        referralLink.shouldBe(visible);
        copyUrlButton.shouldBe(visible);
        progressBar.shouldBe(visible);
        opinionLeadersButton.shouldBe(visible);
        termsButton.shouldBe(visible);
    }

    @Step("Click therms button")
    public void clickThermsButton() {
        termsButton.click();
    }

    @Step("Check visibility therms page elements")
    public void checkVisibilityThermsElements() {
        //pandariumLogoTerms.shouldBe(visible);
        termsTitle.shouldBe(visible);
        termsText.shouldBe(visible);
        termsCloseButton.shouldBe(visible);
    }

    @Step("Click terms close button")
    public void clickThermsCloseButton() {
        termsCloseButton.click();
    }

    @Step("Click opinion leaders button")
    public void clickOpinionLeadersButton() {
        opinionLeadersButton.click();
    }

    @Step("Check visibility opinion leaders elements")
    public void checkVisibilityOpinionElements() {
        pandariumLogo.shouldBe(visible);
        opinionLeadersTitle.shouldBe(visible);
        opinionLeadersText.shouldBe(visible);
        leaderNameField.shouldBe(visible);
        leaderEmailField.shouldBe(visible);
        leaderDescrField.shouldBe(visible);
        sendOpinionButton.shouldBe(visible);
        closeOpinionLeaderButton.shouldBe(visible);
    }

    @Step("Click  close opinion leaders")
    public void clickCloseOpinionLeaderButton() {
        closeOpinionLeaderButton.click();
    }

    @Step("enter name")
    public void enterName() {
        leaderNameField.setValue(NAME);
    }

    @Step("enter email")
    public void enterEmail(String email) {
        leaderEmailField.setValue(email);
    }

    @Step("enter description")
    public void enterDescription() {
        leaderDescrField.setValue(DESCRIPTION);
    }

    @Step("Check 'enter real email' error is visible")
    public void checkEnterRealEmailErorrIsVisible() {
        enterYourRealEmailError.shouldBe(visible);
    }

    @Step("Click send button")
    public void clickSendButton() {
        sendOpinionButton.click();
    }

    @Step("Check 'field is required' error is visible")
    public void fieldIsRequiredErrorIsVisible() {
        fieldIsRequiredError.shouldBe(visible);
    }

    @Step("Click copy url button")
    public void clickCopyUrlButton() {
        copyUrlButton.click();
        urlCopiedMessage.shouldBe(visible);
    }

    @Step("open page from clipboard")
    public void openPageFromClipboard() throws AWTException {
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_T);

        robot.keyPress(KeyEvent.VK_V);

        robot.keyPress(KeyEvent.VK_ENTER);

        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_T);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }

    @Step("check counter")
    public void checkCounter() {
        inviteStatusActive.shouldBe(visible);
        Assert.assertEquals(inviteCountActive.getText(),"1");
    }

    @Step("Check that user has referral email in DB")
    public void checkUserHaveReferralEmailInDB(String email,String refEmail) {
        Assert.assertEquals(refEmail,dataBase.getUserReferralEmail(email));
    }
}
