package Utils;

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.Arrays;

public class DataBase {

    private MongoClientURI connectionString;
    private MongoClient mongoClient;
    private MongoDatabase database;
    String dbHost;
    String dbName = "pandarium";
    String username = "pandarium";
    String password = "pOxEywf6hbhkt8x";
    MongoCredential credential = MongoCredential.createCredential(username, dbName, password.toCharArray());

    public DataBase(String env) {
        if (env==null || env.equalsIgnoreCase("local")){
            dbHost = "127.0.0.1";
        } else if(env.equalsIgnoreCase("dev")){
            dbHost = "dev.pandarium.co";
        }else if(env.equalsIgnoreCase("prod")){
            dbHost = "pandarium.co";
        }
        ServerAddress serverAddress = new ServerAddress(dbHost, ServerAddress.defaultPort());
        mongoClient = new MongoClient(serverAddress, Arrays.asList(credential));
        database = mongoClient.getDatabase("pandarium");
    }



    public Object getIdByEmail(String email) {
        MongoCollection<Document> collection = database.getCollection("prelaunch_users");
        BasicDBObject query=new BasicDBObject("email",email);
        Document myDoc = collection.find(query).first();
        return myDoc.get("_id");
    }

    public String getConfirmTokenByEmail(String email){
        MongoCollection<Document> collection = database.getCollection("prelaunch_users");
        BasicDBObject query= new BasicDBObject("email",email);
        Document myDoc = collection.find(query).first();
        Document prelaunchReg = (Document) myDoc.get("prelaunchRegistration");
        return prelaunchReg.get("confirmToken").toString();
    }

    public String getPrivateTokenByEmail(String email){
        MongoCollection<Document> collection = database.getCollection("prelaunch_users");
        BasicDBObject query= new BasicDBObject("email",email);
        Document myDoc = collection.find(query).first();
        Document prelaunchReg = (Document) myDoc.get("prelaunchRegistration");
        return prelaunchReg.get("privateToken").toString();
    }

    public Boolean getStatusActive(String email){
        MongoCollection<Document> collection = database.getCollection("prelaunch_users");
        BasicDBObject query= new BasicDBObject("email",email);
        Document myDoc = collection.find(query).first();
        Document prelaunchReg = (Document) myDoc.get("prelaunchRegistration");
        return (Boolean) prelaunchReg.get("active");
    }

    public Boolean isUserRegistred(String email){
        MongoCollection<Document> collection = database.getCollection("prelaunch_users");
        BasicDBObject query=new BasicDBObject("email",email);
        Document myDoc = collection.find(query).first();
        if (myDoc!=null){
            return true;
        }
        return false;
    }

    public String getUserReferralEmail(String email) {
        MongoCollection<Document> collection = database.getCollection("prelaunch_users");
        BasicDBObject query = new BasicDBObject("email",email);
        Document myDoc = collection.find(query).first();
        Document prelaunchReg = (Document) myDoc.get("prelaunchRegistration");
        return prelaunchReg.get("referralEmail").toString();
    }
}
