package Utils;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.XmlClientConfigBuilder;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.bson.types.ObjectId;

import java.io.InputStream;


public class Cluster {

    private HazelcastInstance hazelcastClient;
    private DataBase dataBase;
    private InputStream is;
    public Cluster(String environment){
        hazelcastClient = createInstanceClient(environment);
        dataBase = new DataBase(environment);
    }

    public HazelcastInstance createInstanceClient(String environment){

        if (environment==null){
             is = ClassLoader.getSystemResourceAsStream("hazelcast_client_local.xml");
            } else if(environment.equalsIgnoreCase("dev")){
            is = ClassLoader.getSystemResourceAsStream("hazelcast_client_dev.xml");
            } else if(environment.equalsIgnoreCase("local")) {
            is = ClassLoader.getSystemResourceAsStream("hazelcast_client_local.xml");
            } else if(environment.equalsIgnoreCase("prod")){
            is = ClassLoader.getSystemResourceAsStream("hazelcast_client_prod.xml");
        }
        ClientConfig config = new XmlClientConfigBuilder(is).build();
        hazelcastClient = HazelcastClient.newHazelcastClient(config);
        return hazelcastClient;
    }

    public void deleteUserByEmail(String email){

        IMap<ObjectId, ?> prelaunchUsers = hazelcastClient.getMap("prelaunchUsers");
        try {
            prelaunchUsers.remove(dataBase.getIdByEmail(email));
        } catch (NullPointerException e){
            System.out.println("There are no user with email = " + email);
        }
    }
}
