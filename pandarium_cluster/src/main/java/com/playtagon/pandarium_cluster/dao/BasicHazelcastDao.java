package com.playtagon.pandarium_cluster.dao;

import java.util.Collection;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.Predicate;

public class BasicHazelcastDao<K, V> {
	private transient HazelcastInstance hz;
	private transient String mapName;

	public BasicHazelcastDao(HazelcastInstance hz, String mapName) {
		super();
		this.hz = hz;
		this.mapName = mapName;
	}

	public BasicHazelcastDao() {

	}

	public int count() {
		return getMap().size();
	}

	public void removeById(K key) {
		getMap().remove(key);
	}

	public boolean keyExists(K key) {
		return getMap().containsKey(key);
	}

	public boolean valueExists(V value) {
		return getMap().containsValue(value);
	}

	public V getById(K key) {
		return getMap().get(key);
	}

	public void save(K key, V value) {
		getMap().set(key, value);
	}

	public IMap<K, V> getMap() {
		return hz.getMap(mapName);
	}

	public Collection<V> getAllEntites() {
		return getMap().values();
	}

	@SuppressWarnings("rawtypes")
	public Collection<V> getEntitesByPredicate(Predicate predicate) {
		return getMap().values(predicate);
	}

}
