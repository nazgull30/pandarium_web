package com.playtagon.pandarium_cluster.dao;

import java.security.InvalidParameterException;
import java.util.Collection;
import java.util.Optional;

import org.bson.types.ObjectId;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.Predicates;
import com.playtagon.pandarium_entities.Admin;

public class AdminDao extends BasicHazelcastDao<ObjectId, Admin> {

	public AdminDao(HazelcastInstance hz) {
		super(hz, "admins");
	}

	@SuppressWarnings("rawtypes")
	public Admin findAdminByUsername(String username) {
		Predicate predicate = Predicates.equal("username", username);
		Collection<Admin> foundAdmins = getMap().values(predicate);

		if (foundAdmins.size() > 1) {
			throw new InvalidParameterException(
					"There are " + foundAdmins.size() + " admins with same credentials. Should be 0 or 1.");
		}

		Optional<Admin> res = foundAdmins.stream().findFirst();
		return res.isPresent() ? res.get() : null;
	}

	public Admin createNewAdmin(String username, String passwordHash, byte[] salt) {
		Admin admin = new Admin(username, passwordHash, salt);
		admin.setId(new ObjectId());
		save(admin.getId(), admin);
		return admin;
	}

}
