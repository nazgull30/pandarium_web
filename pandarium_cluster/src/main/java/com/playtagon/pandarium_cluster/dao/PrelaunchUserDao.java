package com.playtagon.pandarium_cluster.dao;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.bson.types.ObjectId;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.projection.Projection;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import com.hazelcast.query.Predicates;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration.TokenType;

@SuppressWarnings({ "rawtypes", "serial", "unchecked" })
public class PrelaunchUserDao extends BasicHazelcastDao<ObjectId, PrelaunchUser> implements Serializable {

	public PrelaunchUserDao(HazelcastInstance hz) {
		super(hz, "prelaunchUsers");
	}

	public ObjectId getUserIdByEmail(String email) {
		Predicate predicate = Predicates.equal("email", email);
		Collection<ObjectId> foundIds = getMap().project(new PrelaunchUserIdProjection(), predicate);

		if (foundIds.size() > 1) {
			throw new InvalidParameterException(
					"There are " + foundIds.size() + " users with same emails. Should be 0 or 1.");
		}

		Optional<ObjectId> result = foundIds.stream().findFirst();
		return result.isPresent() ? result.get() : null;
	}

	public boolean checkIfUserWithEmailExists(String email) {
		Predicate predicate = Predicates.equal("email", email);
		Collection<ObjectId> foundIds = getMap().project(new PrelaunchUserIdProjection(), predicate);
		return foundIds.size() > 0;
	}

	public ObjectId getUserIdByToken(TokenType tokenType, String token) {
		String tokenName = "";
		switch (tokenType) {
		case REFERRAL:
			tokenName = "referralToken";
			break;
		case PRESALE:
			tokenName = "presaleToken";
			break;
		case PRIVATE:
			tokenName = "privateToken";
			break;
		case CONFIRM:
			tokenName = "confirmToken";
			break;
		}

		EntryObject e = new PredicateBuilder().getEntryObject();
		Predicate predicate = e.get("prelaunchRegistration." + tokenName).equal(token);

		Collection<ObjectId> foundIds = getMap().project(new PrelaunchUserIdProjection(), predicate);

		if (foundIds.size() > 1) {
			throw new InvalidParameterException(
					"There are " + foundIds.size() + " users with same prelaunch registrations. Should be 0 or 1.");
		}

		Optional<ObjectId> result = foundIds.stream().findFirst();
		return result.isPresent() ? result.get() : null;
	}

	public int count() {
		return getMap().size();
	}

	private class PrelaunchUserIdProjection extends Projection<Map.Entry<ObjectId, PrelaunchUser>, ObjectId>
			implements Serializable {

		@Override
		public ObjectId transform(Entry<ObjectId, PrelaunchUser> input) {
			return input.getKey();
		}

	}
}
