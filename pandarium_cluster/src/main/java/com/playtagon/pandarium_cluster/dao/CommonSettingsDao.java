package com.playtagon.pandarium_cluster.dao;

import com.hazelcast.core.HazelcastInstance;
import com.playtagon.pandarium_entities.CommonSettings;

public class CommonSettingsDao extends BasicHazelcastDao<String, CommonSettings> {

	public CommonSettingsDao(HazelcastInstance hz) {
		super(hz, "commonSettings");
	}

}
