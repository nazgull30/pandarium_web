package com.playtagon.pandarium_cluster.dao;

import com.hazelcast.core.HazelcastInstance;
import com.playtagon.pandarium_entities.UserWallet;

public class UserWalletDao extends BasicHazelcastDao<String, UserWallet> {

	public UserWalletDao(HazelcastInstance hz) {
		super(hz, "wallets");
	}
}
