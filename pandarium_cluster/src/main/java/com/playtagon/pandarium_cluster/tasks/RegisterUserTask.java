package com.playtagon.pandarium_cluster.tasks;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.Callable;

import org.bson.types.ObjectId;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.services.MailService;
import com.playtagon.pandarium_cluster.services.PrelaunchService;
import com.playtagon.pandarium_cluster.tasks.RegisterUserTask.RegisterUserError;
import com.playtagon.pandarium_cluster.utils.Utils;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration.TokenType;

@SuppressWarnings("serial")
public class RegisterUserTask implements Callable<RegisterUserError>, Serializable, HazelcastInstanceAware {

	private transient HazelcastInstance hz;

	private final String email;
	private final String referralEmail;

	public RegisterUserTask(String email, String referralEmail) {
		super();
		this.email = email;
		this.referralEmail = referralEmail;
	}

	@Override
	public void setHazelcastInstance(HazelcastInstance hz) {
		this.hz = hz;
	}

	@Override
	public RegisterUserError call() {
		RegisterUserError error = RegisterUserError.NONE;

		String confirmToken = PrelaunchService.generateTokenByEmail(TokenType.CONFIRM, email);

		createUser(confirmToken);
		String baseUrl = Utils.getBaseUrl(hz);
		MailService mailService = new MailService(baseUrl);
		try {
			mailService.saveToMailChimp(email, confirmToken);

		} catch (Exception e) {
			System.out.println("Email " + email + " has already been registered on mail chimp");
			error = RegisterUserError.EMAIL_ALREADY_REGISTERED;
		}

		boolean status = mailService.sendConfirmationLetter(email, confirmToken);
		error = status ? RegisterUserError.NONE : RegisterUserError.TRANSFER_EMAIL_ERROR;

		return error;
	}

	public ObjectId createUser(String confirmToken) throws IllegalArgumentException {
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		if (userDao.getUserIdByEmail(email) != null)
			throw new IllegalArgumentException();

		PrelaunchUserRegistration prelaunchRegistration = referralEmail != null
				? new PrelaunchUserRegistration(new ObjectId(), referralEmail)
				: new PrelaunchUserRegistration(new ObjectId());

		prelaunchRegistration.setConfirmToken(confirmToken);
		prelaunchRegistration.setRegistrationDate(new Date());

		ObjectId id = new ObjectId();
		PrelaunchUser user = new PrelaunchUser(id, email, prelaunchRegistration);
		userDao.save(id, user);

		return id;
	}

	public enum RegisterUserError {
		NONE, EMAIL_ALREADY_REGISTERED, UNKNOWN, TRANSFER_EMAIL_ERROR
	}

}
