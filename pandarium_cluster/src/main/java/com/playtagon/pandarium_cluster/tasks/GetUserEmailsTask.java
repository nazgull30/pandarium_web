package com.playtagon.pandarium_cluster.tasks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;

import org.bson.types.ObjectId;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;
import com.hazelcast.projection.Projection;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import com.playtagon.pandarium_cluster.aggregators.UserAggregator;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.models.EmailData;
import com.playtagon.pandarium_entities.PrelaunchUser;

@SuppressWarnings("serial")
public class GetUserEmailsTask implements Callable<List<EmailData>>, Serializable, HazelcastInstanceAware {
	private transient HazelcastInstance hz;

	@Override
	public void setHazelcastInstance(HazelcastInstance hz) {
		this.hz = hz;
	}

	@Override
	public List<EmailData> call() throws Exception {
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		Collection<EmailData> emailDatas = getAllUserEmails(userDao);
		System.out.println("emailDatas: " + emailDatas.size());

		List<EmailData> result = new ArrayList<>();

		for (EmailData emailData : emailDatas) {
			long totalFriends = getUserFriendsCount(emailData.getEmail(), false, userDao);
			long verifiedFriends = getUserFriendsCount(emailData.getEmail(), true, userDao);
			emailData.setTotalFriends(totalFriends);
			emailData.setVerifiedFriends(verifiedFriends);
			result.add(emailData);
		}

		return result;
	}

	@SuppressWarnings({ "unchecked" })
	public Collection<EmailData> getAllUserEmails(PrelaunchUserDao userDao) {
		EntryObject e = new PredicateBuilder().getEntryObject();
		PredicateBuilder predicate = e.get("prelaunchRegistration").isNotNull();
		Collection<EmailData> emailDatas = userDao.getMap().project(new EmailProjection(), predicate);
		return emailDatas;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public long getUserFriendsCount(String email, boolean onlyConfirmed, PrelaunchUserDao userDao) {
		EntryObject e = new PredicateBuilder().getEntryObject();
		PredicateBuilder refPred = e.get("prelaunchRegistration.referralEmail").equal(email);
		PredicateBuilder activePred = e.is("prelaunchRegistration.active");
		Predicate predicate = onlyConfirmed ? activePred.and(refPred) : refPred;
		Long res = userDao.getMap().aggregate(new UserAggregator(), predicate);
		return res.longValue();
	}

	private class EmailProjection extends Projection<Map.Entry<ObjectId, PrelaunchUser>, EmailData> {
		@Override
		public EmailData transform(Entry<ObjectId, PrelaunchUser> input) {
			String email = input.getValue().getEmail();
			Date registeredDate = input.getValue().getPrelaunchRegistration().getRegistrationDate();
			EmailData emailData = new EmailData(email, input.getValue().getPrelaunchRegistration().isPresaleSent(),
					registeredDate, input.getValue().getPrelaunchRegistration().isActive());
			return emailData;
		}
	}
}
