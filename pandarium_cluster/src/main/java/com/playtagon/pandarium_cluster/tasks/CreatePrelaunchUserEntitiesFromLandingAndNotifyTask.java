package com.playtagon.pandarium_cluster.tasks;

import java.io.Serializable;
import java.util.List;

import org.bson.types.ObjectId;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;
import com.hazelcast.core.IMap;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.services.MailService;
import com.playtagon.pandarium_cluster.services.PrelaunchService;
import com.playtagon.pandarium_cluster.utils.Utils;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration.TokenType;

import model.list.member.Member;

@SuppressWarnings("serial")
public class CreatePrelaunchUserEntitiesFromLandingAndNotifyTask
		implements Runnable, Serializable, HazelcastInstanceAware {

	private transient HazelcastInstance hz;
	private int memberCount; // 0 - retrieve all members
	private boolean sendLetter;

	public CreatePrelaunchUserEntitiesFromLandingAndNotifyTask(int memberCount, boolean sendLetter) {
		super();
		this.memberCount = memberCount;
		this.sendLetter = sendLetter;
	}

	public CreatePrelaunchUserEntitiesFromLandingAndNotifyTask(boolean sendLetter) {
		super();
		this.sendLetter = sendLetter;
	}

	@Override
	public void setHazelcastInstance(HazelcastInstance hz) {
		this.hz = hz;
	}

	@Override
	public void run() {
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);

		String baseUrl = Utils.getBaseUrl(hz);
		MailService mailService = new MailService(baseUrl);
		try {
			System.out.println("CreatePrelaunchUserEntitiesFromLandingAndNotifyTask run");

			List<Member> subscribers = mailService.getPandariumSubscribersFromLandingPage(memberCount);
			System.out.println("subscribers from landing, count: " + subscribers.size());

			// addSubscriberAndSendLetter(subscribers.get(0), userDao, mailService,
			// sendLetter);
			// }

			for (Member member : subscribers) {
				addSubscriberAndSendLetter(member, userDao, mailService, sendLetter);
			}

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		IMap<ObjectId, PrelaunchUser> prelaunchUsers = hz.getMap("prelaunchUsers");
		System.out.println("size:" + prelaunchUsers.size());

	}

	private String createPrelaunchUser(String email) {
		String confirmToken = PrelaunchService.generateTokenByEmail(TokenType.CONFIRM, email);
		RegisterUserTask registerUserTask = new RegisterUserTask(email, "");
		registerUserTask.setHazelcastInstance(hz);
		registerUserTask.createUser(confirmToken);
		return confirmToken;
	}

	public void addSubscriberAndSendLetter(Member subscriber, PrelaunchUserDao userDao, MailService mailService,
			boolean sendLetter) {
		String email = subscriber.getEmail_address();

		if (email.toLowerCase().contains("test")) // skip all test emails
			return;

		ObjectId userId = userDao.getUserIdByEmail(email);
		if (userId != null)
			return;

		String confirmToken = createPrelaunchUser(email);

		if (sendLetter) {
			mailService.sendConfirmationLetter(email, confirmToken);
		}
		System.out.println("USER " + email);

	}

}
