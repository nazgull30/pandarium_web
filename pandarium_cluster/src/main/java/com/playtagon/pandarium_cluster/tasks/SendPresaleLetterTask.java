package com.playtagon.pandarium_cluster.tasks;

import java.io.Serializable;
import java.util.Collection;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;
import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.services.MailService;
import com.playtagon.pandarium_cluster.services.PrelaunchService;
import com.playtagon.pandarium_cluster.utils.Utils;
import com.playtagon.pandarium_entities.CommonSettings;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration.TokenType;

@SuppressWarnings("serial")
public class SendPresaleLetterTask implements Runnable, Serializable, HazelcastInstanceAware {

	private final Collection<String> emails;

	public SendPresaleLetterTask(Collection<String> emails) {
		super();
		this.emails = emails;
	}

	private transient HazelcastInstance hz;

	@Override
	public void setHazelcastInstance(HazelcastInstance hz) {
		this.hz = hz;
	}

	@Override
	public void run() {
		CommonSettingsDao commonSettingsDao = new CommonSettingsDao(hz);
		CommonSettings adminEmail = commonSettingsDao.getById("adminEmail");

		int totalEmailCount = emails.size();
		int deliveredEmailCount = 0;
		String baseUrl = Utils.getBaseUrl(hz);
		MailService mailService = new MailService(baseUrl);
		for (String email : emails) {
			String presaleToken = PrelaunchService.generateTokenByEmail(TokenType.CONFIRM, email);
			boolean status = mailService.sendPresaleLetter(email, presaleToken);
			if (status) {
				deliveredEmailCount++;
			}
		}
		mailService.sendPresaleResultLetterToAdmin(totalEmailCount, deliveredEmailCount, adminEmail.getValue());
	}

}
