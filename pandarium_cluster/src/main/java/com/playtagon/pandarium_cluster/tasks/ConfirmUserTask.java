package com.playtagon.pandarium_cluster.tasks;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;

import org.bson.types.ObjectId;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;
import com.hazelcast.projection.Projection;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.Predicates;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.services.MailService;
import com.playtagon.pandarium_cluster.services.PrelaunchService;
import com.playtagon.pandarium_cluster.tasks.ConfirmUserTask.ConfirmUserResult;
import com.playtagon.pandarium_cluster.utils.Utils;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration.TokenType;

import lombok.AllArgsConstructor;
import lombok.Data;

@SuppressWarnings("serial")
public class ConfirmUserTask implements Callable<ConfirmUserResult>, Serializable, HazelcastInstanceAware {
	private transient HazelcastInstance hz;
	private transient PrelaunchUserDao userDao;

	private final String confirmToken;

	public ConfirmUserTask(String confirmToken) {
		super();
		this.confirmToken = confirmToken;
	}

	@Override
	public void setHazelcastInstance(HazelcastInstance hz) {
		this.hz = hz;
	}

	@Override
	public ConfirmUserResult call() {
		userDao = new PrelaunchUserDao(hz);
		PrelaunchService prelauchSerive = new PrelaunchService(userDao);

		String baseUrl = Utils.getBaseUrl(hz);

		ObjectId userId = userDao.getUserIdByToken(TokenType.CONFIRM, confirmToken);
		if (userId == null)
			return new ConfirmUserResult(null, false, baseUrl);

		if (!prelauchSerive.activateUser(userId))
			return new ConfirmUserResult(null, false, baseUrl);

		UserInfo info = getEmailInfo(userId);
		boolean confirmStatus = sendEmailToUser(userId, info, baseUrl);

		return new ConfirmUserResult(info, confirmStatus, baseUrl);
	}

	private boolean sendEmailToUser(ObjectId userId, UserInfo info, String baseUrl) {
		MailService mailService = new MailService(baseUrl);
		return mailService.sendActivateLetter(info.getEmail(), info.getReferralToken(), info.getPrivateToken());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private UserInfo getEmailInfo(ObjectId userId) {
		Predicate predicate = Predicates.equal("id", userId);
		Collection<UserInfo> userInfos = userDao.getMap()
				.project(new Projection<Map.Entry<ObjectId, PrelaunchUser>, UserInfo>() {

					@Override
					public UserInfo transform(Entry<ObjectId, PrelaunchUser> input) {
						return new UserInfo(input.getValue().getEmail(),
								input.getValue().getPrelaunchRegistration().getReferralToken(),
								input.getValue().getPrelaunchRegistration().getPrivateToken());
					}
				}, predicate);

		if (userInfos.size() > 1) {
			throw new InvalidParameterException("There are " + userInfos.size()
					+ " users with same email/prelaunch registrations. Should be 0 or 1.");
		}

		return userInfos.stream().findFirst().get();
	}

	@Data @AllArgsConstructor
	public class ConfirmUserResult implements Serializable {
		private UserInfo userInfo;
		private boolean sendConfirmStatus;
		private String baseUrl;
		
		public String getPrivateLink() {
			return userInfo != null ? baseUrl + "progress/" + userInfo.getPrivateToken() : "";
		}

		public String getReferralLink() {
			return userInfo != null ? baseUrl + "reg/" + userInfo.getReferralToken() : "";
		}

	}
	
	@Data @AllArgsConstructor
	public class UserInfo implements Serializable {
		private String email;
		private String referralToken;
		private String privateToken;

	}

}
