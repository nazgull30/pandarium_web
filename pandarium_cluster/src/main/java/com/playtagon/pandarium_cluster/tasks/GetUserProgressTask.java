package com.playtagon.pandarium_cluster.tasks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;
import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.models.FriendInfo;
import com.playtagon.pandarium_cluster.models.PrelaunchRewardParams;
import com.playtagon.pandarium_cluster.models.UserProgress;
import com.playtagon.pandarium_cluster.services.PrelaunchService;
import com.playtagon.pandarium_cluster.services.PrelaunchUserService;
import com.playtagon.pandarium_cluster.utils.Utils;

@SuppressWarnings("serial")
public class GetUserProgressTask implements Callable<UserProgress>, Serializable, HazelcastInstanceAware {
	private transient HazelcastInstance hz;
	private transient PrelaunchUserDao userDao;

	private final String privateToken;

	public GetUserProgressTask(String privateToken) {
		super();
		this.privateToken = privateToken;
	}

	@Override
	public void setHazelcastInstance(HazelcastInstance hz) {
		this.hz = hz;
	}

	@Override
	public UserProgress call() throws Exception {
		userDao = new PrelaunchUserDao(hz);
		PrelaunchUserService userService = new PrelaunchUserService(userDao);
		String email = userService.getUserEmailByPrivateToken(privateToken);

		System.out.println("email: " + email);

		if (email == null)
			return null;

		Collection<FriendInfo> friends = userService.getAllFriendsByUserEmail(email);

		CommonSettingsDao settingsDao = new CommonSettingsDao(hz);
		PrelaunchService prelaunchService = new PrelaunchService(settingsDao);
		PrelaunchRewardParams prelaunchRewardParams = prelaunchService.getRewardParams();

		String baseUrl = Utils.getBaseUrl(hz);
		UserProgress userProgress = new UserProgress(baseUrl);
		userProgress.setFriends(new ArrayList<>(friends));

		String refferalToken = userService.getReferralTokenByUserEmail(email);
		userProgress.setReferralToken(refferalToken);

		long activeFriends = userProgress.getActiveFriends();

		int pandaRewards = prelaunchRewardParams.calculatePandaPromoCount(activeFriends);
		userProgress.setPandaRewards(pandaRewards);
		return userProgress;
	}

}
