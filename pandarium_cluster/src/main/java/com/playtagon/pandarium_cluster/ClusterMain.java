package com.playtagon.pandarium_cluster;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletFile;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.infura.InfuraHttpService;

import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.playtagon.pandarium_cluster.contracts.PresaleCampaign;
import com.playtagon.pandarium_cluster.contracts.PresaleCampaign.PandaSoldEventResponse;
import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.services.AdminService;
import com.playtagon.pandarium_entities.CommonSettings;

public class ClusterMain {
	private Logger logger = LoggerFactory.getLogger(ClusterMain.class);

	private HazelcastInstance hazelcast;

	public static void main(String[] args) {
		try {
			System.setProperty("-Djava.util.logging.config.file", "logging.properties");
			String group = args[0];
			ClusterMain clusterMain = new ClusterMain();
			clusterMain.start(group);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void start(String group) throws FileNotFoundException {
		InputStream is = ClassLoader.getSystemResourceAsStream("hazelcast_server.xml");
		Config config = new XmlConfigBuilder(is).build();
		config.getGroupConfig().setName(group);
		hazelcast = Hazelcast.newHazelcastInstance(config);

		logger.info("admins size: " + hazelcast.getMap("admins").size());
		logger.info("common settins size: " + hazelcast.getMap("commonSettings").size());
	}

	public void shutdown() {
		logger.info("Cluster: shutdown");
		hazelcast.shutdown();
	}

	public HazelcastInstance getHazelcast() {
		return hazelcast;
	}

	private void startWeb3jEvents() {

		CommonSettingsDao settingsDao = new CommonSettingsDao(hazelcast);
		CommonSettings contractAddress = settingsDao.getById("presaleContractAddress");

		if (contractAddress == null)
			return;

		WalletFile ceoWallet = new AdminService(settingsDao).loadCeoWallet();
		if (ceoWallet == null)
			return;

		CommonSettings ethereumNode = settingsDao.getById("ethereumNode");
		if (ethereumNode == null)
			return;

		Web3j web3j = Web3j.build(new InfuraHttpService(ethereumNode.getValue()));

		String presaleContractAddress = contractAddress.getValue();

		File file = new File(getClass().getClassLoader().getResource("wallets/simpleWallet.json").getFile());
		Credentials credentials = null;
		try {
			credentials = WalletUtils.loadCredentials("Playtagon12345", file);
		} catch (IOException | CipherException e) {
			e.printStackTrace();
		}

		PresaleCampaign presaleCampaign = PresaleCampaign.load(presaleContractAddress, web3j, credentials,
				BigInteger.valueOf(10000), BigInteger.valueOf(470000));

		try {
			logger.info("Presale contract is valid: " + presaleCampaign.isValid());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		presaleCampaign.pandaSoldEventObservable(DefaultBlockParameterName.LATEST, DefaultBlockParameterName.LATEST)
				.subscribe(r -> storeTransaction(r));
	}

	private void storeTransaction(PandaSoldEventResponse response) {
		IMap<ObjectId, PandaSoldEventResponse> soldEvents = hazelcast.getMap("soldEvents");
		soldEvents.set(new ObjectId(), response);
		logger.info("Buyer " + response.wallet + " made a booking");
	}

}
