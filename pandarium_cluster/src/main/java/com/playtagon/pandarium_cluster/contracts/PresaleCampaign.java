package com.playtagon.pandarium_cluster.contracts;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.EventValues;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint16;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.1.1.
 */
public final class PresaleCampaign extends Contract {
    private static final String BINARY = "0x6060604052336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506107a3806100536000396000f3006060604052600436106100a4576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680630fb5a6b4146102965780632eefe0e5146102bf578063528e4959146103105780636576d3bf14610341578063679cf5ee146103965780638da5cb5b146103c7578063c4a2eb751461041c578063eba0475f14610471578063f21f537d1461049a578063f2fde38b146104c3575b600060025460015401421015156100ba57600080fd5b6000600354348115156100c957fe5b061415156100d657600080fd5b600354348115156100e357fe5b049050600460029054906101000a900461ffff1661ffff168161ffff161115151561010d57600080fd5b600073ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415151561014957600080fd5b80600560003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282829054906101000a900461ffff160192506101000a81548161ffff021916908361ffff16021790555080600460028282829054906101000a900461ffff160392506101000a81548161ffff021916908361ffff1602179055507fc8fb86732e369fd86f92f6eba411521121062aa6e9b28732bf8f7944edc0a60333604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390a16000600460029054906101000a900461ffff1661ffff161415610293577f52df9fe5b9c9a7b0b4fdc2c9f89387959e35e4209c2a8d133a2b8165edad2a0460405160405180910390a15b50005b34156102a157600080fd5b6102a96104fc565b6040518082815260200191505060405180910390f35b34156102ca57600080fd5b6102f6600480803561ffff16906020019091908035906020019091908035906020019091905050610502565b604051808215151515815260200191505060405180910390f35b341561031b57600080fd5b6103236105d1565b604051808261ffff1661ffff16815260200191505060405180910390f35b341561034c57600080fd5b610378600480803573ffffffffffffffffffffffffffffffffffffffff169060200190919050506105e5565b604051808261ffff1661ffff16815260200191505060405180910390f35b34156103a157600080fd5b6103a9610606565b604051808261ffff1661ffff16815260200191505060405180910390f35b34156103d257600080fd5b6103da61061a565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561042757600080fd5b610453600480803573ffffffffffffffffffffffffffffffffffffffff1690602001909190505061063f565b604051808261ffff1661ffff16815260200191505060405180910390f35b341561047c57600080fd5b610484610696565b6040518082815260200191505060405180910390f35b34156104a557600080fd5b6104ad61069c565b6040518082815260200191505060405180910390f35b34156104ce57600080fd5b6104fa600480803573ffffffffffffffffffffffffffffffffffffffff169060200190919050506106a2565b005b60025481565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561055f57600080fd5b60025460015401421015151561057457600080fd5b83600460006101000a81548161ffff021916908361ffff16021790555083600460026101000a81548161ffff021916908361ffff1602179055506001905042600181905550603c8302600281905550816003819055509392505050565b600460009054906101000a900461ffff1681565b60056020528060005260406000206000915054906101000a900461ffff1681565b600460029054906101000a900461ffff1681565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b6000600560008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900461ffff169050919050565b60035481565b60015481565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156106fd57600080fd5b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff1614151561077457806000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055505b505600a165627a7a72305820c78fc15c6a89fa4f51e0fe23c34f0ef85e62f017e786eaff8812e5cd14e8d5b80029";

    protected static final HashMap<String, String> _addresses;

    static {
        _addresses = new HashMap<>();
        _addresses.put("3", "0x65ab52006be3d19fffc985c1e627951f8d217e5b");
    }

    private PresaleCampaign(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    private PresaleCampaign(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public List<PandaSoldEventResponse> getPandaSoldEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("PandaSold", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        List<EventValues> valueList = extractEventParameters(event, transactionReceipt);
        ArrayList<PandaSoldEventResponse> responses = new ArrayList<PandaSoldEventResponse>(valueList.size());
        for (EventValues eventValues : valueList) {
            PandaSoldEventResponse typedResponse = new PandaSoldEventResponse();
            typedResponse.wallet = (String) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<PandaSoldEventResponse> pandaSoldEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("PandaSold", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, PandaSoldEventResponse>() {
            @Override
            public PandaSoldEventResponse call(Log log) {
                EventValues eventValues = extractEventParameters(event, log);
                PandaSoldEventResponse typedResponse = new PandaSoldEventResponse();
                typedResponse.wallet = (String) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public List<SoldOutEventResponse> getSoldOutEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("SoldOut", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList());
        List<EventValues> valueList = extractEventParameters(event, transactionReceipt);
        ArrayList<SoldOutEventResponse> responses = new ArrayList<SoldOutEventResponse>(valueList.size());
        for (EventValues eventValues : valueList) {
            SoldOutEventResponse typedResponse = new SoldOutEventResponse();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<SoldOutEventResponse> soldOutEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("SoldOut", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList());
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, SoldOutEventResponse>() {
            @Override
            public SoldOutEventResponse call(Log log) {
                EventValues eventValues = extractEventParameters(event, log);
                SoldOutEventResponse typedResponse = new SoldOutEventResponse();
                return typedResponse;
            }
        });
    }

    public RemoteCall<BigInteger> duration() {
        Function function = new Function("duration", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> start(BigInteger totalPandas, BigInteger durationInMinutes, BigInteger pandaPrice) {
        Function function = new Function(
                "start", 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint16(totalPandas), 
                new org.web3j.abi.datatypes.generated.Uint256(durationInMinutes), 
                new org.web3j.abi.datatypes.generated.Uint256(pandaPrice)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<BigInteger> initialPresalePandas() {
        Function function = new Function("initialPresalePandas", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint16>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> addressPandasCount(String param0) {
        Function function = new Function("addressPandasCount", 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(param0)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint16>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> pandasLeft() {
        Function function = new Function("pandasLeft", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint16>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<String> owner() {
        Function function = new Function("owner", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<BigInteger> getPandasCountFor(String wallet) {
        Function function = new Function("getPandasCountFor", 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(wallet)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint16>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> priceForOnePanda() {
        Function function = new Function("priceForOnePanda", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> startedAt() {
        Function function = new Function("startedAt", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> transferOwnership(String newOwner) {
        Function function = new Function(
                "transferOwnership", 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(newOwner)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public static RemoteCall<PresaleCampaign> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(PresaleCampaign.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<PresaleCampaign> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(PresaleCampaign.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public static PresaleCampaign load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new PresaleCampaign(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static PresaleCampaign load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new PresaleCampaign(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected String getStaticDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static class PandaSoldEventResponse {
        public String wallet;
    }

    public static class SoldOutEventResponse {
    }
}
