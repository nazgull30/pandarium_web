package com.playtagon.pandarium_cluster.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@SuppressWarnings("serial")
@XmlRootElement
@Data
public class PrelaunchRewardParams implements Serializable {
	private int userCountGettingPandas;
	private int friendCountThreshold;
	private int rewardPandaCount;

	public int calculatePandaPromoCount(long friendCount) {
		return (friendCount >= friendCountThreshold) ? rewardPandaCount : 0;
	}
}
