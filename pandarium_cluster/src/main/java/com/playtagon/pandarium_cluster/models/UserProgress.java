package com.playtagon.pandarium_cluster.models;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
public class UserProgress implements Serializable {
	@Getter
	@Setter
	private List<FriendInfo> friends;
	@Getter
	@Setter
	private int pandaRewards;
	@Setter
	private String referralToken;
	private String baseUrl;

	public UserProgress(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public long getActiveFriends() {
		return friends.stream().filter(f -> f.isActive()).count();
	}

	public String getReferralLink() {
		return baseUrl + "reg/" + referralToken;
	}
}
