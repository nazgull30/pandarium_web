package com.playtagon.pandarium_cluster.models;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class EmailData implements Serializable {
	private static final long serialVersionUID = -3964531237668469689L;

	private boolean sendRequired;
	private String email;
	private boolean sent;
	private Date registeredDate;
	private boolean verified;
	private long totalFriends;
	private long verifiedFriends;

	public EmailData(String email, boolean sent, Date registeredDate, boolean verified) {
		super();
		this.email = email;
		this.sent = sent;
		this.registeredDate = registeredDate;
		this.verified = verified;
	}
}
