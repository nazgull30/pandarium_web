package com.playtagon.pandarium_cluster.models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;

import lombok.Getter;

@Getter
@SuppressWarnings("serial")
public class FriendInfo implements Serializable {
	private ObjectId id;
	private String identifier;	// ID that will be shown to user
	private boolean active;
	private String registrationDate;

	public FriendInfo(ObjectId id, boolean active, Date registrationDate) {
		super();
		this.id = id;
		this.active = active;
		this.registrationDate = new SimpleDateFormat("d MMM. yyyy").format(registrationDate);
		this.identifier = "#" + id.toString().substring(19);
	}
}