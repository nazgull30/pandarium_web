package com.playtagon.pandarium_cluster.utils.createlandingusers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IExecutorService;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.services.MailService;
import com.playtagon.pandarium_cluster.tasks.CreatePrelaunchUserEntitiesFromLandingAndNotifyTask;
import com.playtagon.pandarium_cluster.utils.Utils;

import model.list.member.Member;

public class CreateUsersFromLandingTool {

	public static void main(String[] args) throws IOException {
		CreateUsersFromLandingTool tool = new CreateUsersFromLandingTool();

		while (true) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String cmd = reader.readLine();

			switch (cmd) {
			case "init":
				tool.init();
				break;
			case "cpu":
				tool.createPrelaunchUsers();
				break;
			case "ctu":
				tool.createUser();
				break;
			default:
				break;
			}

		}
	}

	private HazelcastInstance hz;

	public void init() throws FileNotFoundException {
		InputStream is = ClassLoader.getSystemResourceAsStream("hazelcast_server_prod_remote.xml");
		Config config = new XmlConfigBuilder(is).build();
		hz = Hazelcast.newHazelcastInstance(config);

		int prelaunchUserCount = hz.getMap("prelaunchUsers").size();

		System.out.println("=======Init clusters========");
		System.out.println("prelaunchUserCount: " + prelaunchUserCount);
		System.out.println("============================");
	}

	public void createPrelaunchUsers() {
		IExecutorService executor = hz.getExecutorService("default");
		executor.execute(new CreatePrelaunchUserEntitiesFromLandingAndNotifyTask(10, true));
	}

	public void createUser() {
		
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		MailService mailService = new MailService(Utils.getBaseUrl(hz));
		
		try {
			Member subscriber =  mailService.getPandariumSubscribersFromLandingPage(1).get(0);
			System.out.println("s: " + subscriber.getEmail_address() + ", id: " + subscriber.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//
//		Member subscriber;
//		try {
//			subscriber = mailService.getSubscriberById("131202193"); // mail: nazgull30
//			new CreatePrelaunchUserEntitiesFromLandingAndNotifyTask(true).addSubscriberAndSendLetter(subscriber,
//					userDao, mailService, true);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

	}

}
