package com.playtagon.pandarium_cluster.utils;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

public class MorphiaUtil {
	private static MorphiaUtil instance;

	private Datastore datastore;

	private MorphiaUtil() {
	}

	public static MorphiaUtil getInstance() {
		if (instance == null) {
			instance = new MorphiaUtil();
		}
		return instance;
	}

	public Datastore getDatastore() {
		if (datastore == null) {
			MongoClient mongoClient = new MongoClient("localhost");
			Morphia morphia = new Morphia();
			datastore = morphia.createDatastore(mongoClient, "pandarium");
		}
		return datastore;
	}

	public void setDatastore(Datastore datastore) {
		this.datastore = datastore;
	}

}
