package com.playtagon.pandarium_cluster.utils;

import java.io.InputStream;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.XmlClientConfigBuilder;
import com.hazelcast.core.HazelcastInstance;

public class HazelcastUtil {
	private static HazelcastUtil instance;

	private HazelcastInstance hazelcastClient;

	private HazelcastUtil() {
	}

	public static HazelcastUtil getInstance() {
		if (instance == null) {
			instance = new HazelcastUtil();
		}
		return instance;
	}

	public HazelcastInstance getHazelcastClient() {
		if (hazelcastClient == null) {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("hazelcast_client.xml");
			ClientConfig config = new XmlClientConfigBuilder(is).build();
			hazelcastClient = HazelcastClient.newHazelcastClient(config);
			System.out.println("ctor cluster utils");
		}
		return hazelcastClient;
	}

	public void setHazelcastInstance(HazelcastInstance hz) {
		hazelcastClient = hz;
	}
}
