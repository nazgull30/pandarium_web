package com.playtagon.pandarium_cluster.utils.databasemigration;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;

import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.playtagon.pandarium_entities.PrelaunchUser;

public class DatabaseMigrationTool {

	public static void main(String[] args) throws IOException {
		DatabaseMigrationTool tool = new DatabaseMigrationTool();
		while (true) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String cmd = reader.readLine();

			switch (cmd) {
			case "init":
				tool.init();
				break;
			case "cse":
				List<String> sameEmails = tool.checkSameEmails();
				System.out.println("=======Same emails========");
				for (String email : sameEmails) {
					System.out.println(email);
				}
				System.out.println("");
				System.out.println("same emails: " + sameEmails);
				System.out.println("============================");
				break;
			case "migrate":
				tool.migratePrelaunchUsers();
				break;
			default:
				break;
			}

		}
	}

	private HazelcastInstance oldCluster;
	private HazelcastInstance newCluster;

	public void init() throws FileNotFoundException {
		oldCluster = createClusterForOldDb();
		newCluster = createClusterForNewDb();

		int oldPrelaunchUserCount = oldCluster.getMap("prelaunchUsers").size();
		int newPrelaunchUserCount = newCluster.getMap("prelaunchUsers").size();

		System.out.println("=======Init clusters========");
		System.out.println("oldPrelaunchUserCount: " + oldPrelaunchUserCount);
		System.out.println("newPrelaunchUserCount: " + newPrelaunchUserCount);
		System.out.println("============================");
	}

	private HazelcastInstance createClusterForOldDb() {
		InputStream is = getClass().getResourceAsStream("hz_old_db.xml");
		Config config = new XmlConfigBuilder(is).build();
		return Hazelcast.newHazelcastInstance(config);
	}

	private HazelcastInstance createClusterForNewDb() {
		InputStream is = getClass().getResourceAsStream("hz_new_db.xml");
		Config config = new XmlConfigBuilder(is).build();
		return Hazelcast.newHazelcastInstance(config);
	}

	public int migratePrelaunchUsers() {
		List<String> sameEmails = checkSameEmails();

		IMap<ObjectId, PrelaunchUser> oldPrelaunchUsers = oldCluster.getMap("prelaunchUsers");
		Collection<PrelaunchUser> oldPrelaunchUsersCol = oldPrelaunchUsers.values();

		List<PrelaunchUser> sameUsers = oldPrelaunchUsersCol.stream().filter(u -> sameEmails.contains(u.getEmail()))
				.collect(Collectors.toList());

		for (PrelaunchUser userToExclude : sameUsers) {
			System.out.println(userToExclude.getEmail());
			oldPrelaunchUsersCol.remove(userToExclude);
		}

		IMap<ObjectId, PrelaunchUser> newPrelaunchUsers = newCluster.getMap("prelaunchUsers");
		for (PrelaunchUser prelaunchUser : oldPrelaunchUsersCol) {
			newPrelaunchUsers.set(prelaunchUser.getId(), prelaunchUser);
		}
		System.out.println("======MIGRATE COMPLETE====");
		System.out.println("newPrelaunchUserCount: " + newPrelaunchUsers.size());

		return newPrelaunchUsers.size();
	}

	public List<String> checkSameEmails() {
		IMap<ObjectId, PrelaunchUser> oldPrelaunchUsers = oldCluster.getMap("prelaunchUsers");
		Collection<PrelaunchUser> oldPrelaunchUsersCol = oldPrelaunchUsers.values();
		Collection<String> oldUserEmails = oldPrelaunchUsersCol.stream().map(u -> u.getEmail())
				.collect(Collectors.toList());

		IMap<ObjectId, PrelaunchUser> newPrelaunchUsers = newCluster.getMap("prelaunchUsers");
		Collection<PrelaunchUser> newPrelaunchUsersCol = newPrelaunchUsers.values();
		Collection<String> newUserEmails = newPrelaunchUsersCol.stream().map(u -> u.getEmail())
				.collect(Collectors.toList());

		List<String> sameEmails = newUserEmails.stream().filter(e -> oldUserEmails.contains(e))
				.collect(Collectors.toList());

		return sameEmails;
	}

	public HazelcastInstance getOldCluster() {
		return oldCluster;
	}

	public HazelcastInstance getNewCluster() {
		return newCluster;
	}

	public void printClustersInfo() {
		int oldPrelaunchUserCount = oldCluster.getMap("prelaunchUsers").size();
		int newPrelaunchUserCount = newCluster.getMap("prelaunchUsers").size();

		System.out.println("=======Clusters info========");
		System.out.println("oldPrelaunchUserCount: " + oldPrelaunchUserCount);
		System.out.println("newPrelaunchUserCount: " + newPrelaunchUserCount);
		System.out.println("============================");
	}

	public void shutdown() {
		oldCluster.shutdown();
		newCluster.shutdown();
	}

}
