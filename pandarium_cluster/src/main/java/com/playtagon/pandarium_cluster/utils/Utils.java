package com.playtagon.pandarium_cluster.utils;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.playtagon.pandarium_entities.CommonSettings;

public class Utils {

	public static String getBaseUrl(HazelcastInstance hz) {
		IMap<String, CommonSettings> settings = hz.getMap("commonSettings");
		CommonSettings baseUrl = settings.get("baseUrl");
		return baseUrl != null ? baseUrl.getValue() : null;
	}
}
