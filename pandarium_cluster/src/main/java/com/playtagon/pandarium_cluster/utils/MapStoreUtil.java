package com.playtagon.pandarium_cluster.utils;

import java.util.Arrays;
import java.util.Properties;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

public class MapStoreUtil {

	public static Datastore createDatastoreByProperties(Properties properties) {
		Morphia morphia = new Morphia();
		String dbHost = (String) properties.getProperty("mongo.host");
		String dbName = (String) properties.get("mongo.db");
		String username = (String) properties.get("mongo.username");
		String password = (String) properties.get("mongo.pass");
		MongoCredential credential = MongoCredential.createCredential(username, dbName, password.toCharArray());
		ServerAddress serverAddress = new ServerAddress(dbHost, ServerAddress.defaultPort());
		MongoClient mongoClient = new MongoClient(serverAddress, Arrays.asList(credential));
		Datastore ds = morphia.createDatastore(mongoClient, dbName);
		return ds;
	}
}
