package com.playtagon.pandarium_cluster.services;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.bson.json.JsonParseException;
import org.web3j.crypto.WalletFile;

import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.models.PrelaunchRewardParams;
import com.playtagon.pandarium_entities.CommonSettings;

public class AdminService {
	private final String CEO_WALLET = "ceoWallet";

	private transient CommonSettingsDao commonSettingsDao;

	public AdminService(CommonSettingsDao commonSettingsDao) {
		super();
		this.commonSettingsDao = commonSettingsDao;
	}

	public void savePelaunchRewardParameters(PrelaunchRewardParams params) {
		StringWriter stringXml = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(PrelaunchRewardParams.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.marshal(params, stringXml);
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		CommonSettings kvSettings = new CommonSettings("prelaunchRewardParams", stringXml.toString());
		commonSettingsDao.save(kvSettings.getKey(), kvSettings);
	}

	public Collection<CommonSettings> createOrUpdateSettings(String key, String value) {
		// CommonSettings settings = commonSettingsDao.getById(key);
		// if (settings == null) {
		//
		// }
		CommonSettings settings = new CommonSettings(key, value);
		commonSettingsDao.save(settings.getKey(), settings);
		return commonSettingsDao.getMap().values();
	}

	public WalletFile trySaveCeoWallet(byte[] walletBytes) {
		WalletFile walletFile = null;
		try {
			CommonSettings ceoWalletSettings = new CommonSettings(CEO_WALLET, new String(walletBytes));
			commonSettingsDao.save(CEO_WALLET, ceoWalletSettings);
			walletFile = WalletService.convertByteArrayToWalletFile(walletBytes);
		} catch (JsonParseException | IOException e) {
			e.printStackTrace();
		}
		return walletFile;
	}

	public WalletFile loadCeoWallet() {
		CommonSettings ceoWalletSettings = commonSettingsDao.getById(CEO_WALLET);
		if (ceoWalletSettings == null)
			return null;

		try {
			return WalletService.convertByteArrayToWalletFile(ceoWalletSettings.getValue().getBytes());
		} catch (JsonParseException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
