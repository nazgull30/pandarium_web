package com.playtagon.pandarium_cluster.services;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.bson.types.ObjectId;

import com.hazelcast.projection.Projection;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import com.hazelcast.query.Predicates;
import com.playtagon.pandarium_cluster.aggregators.UserAggregator;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.models.FriendInfo;
import com.playtagon.pandarium_entities.PrelaunchUser;

@SuppressWarnings({ "rawtypes", "unchecked", "serial" })
public class PrelaunchUserService implements Serializable {
	private transient PrelaunchUserDao userDao;

	public PrelaunchUserService(PrelaunchUserDao userDao) {
		super();
		this.userDao = userDao;
	}

	public String getReferralTokenByUserId(ObjectId userId) {
		Predicate predicate = Predicates.equal("id", userId);
		Collection<String> tokens = userDao.getMap()
				.project(new Projection<Map.Entry<ObjectId, PrelaunchUser>, String>() {

					@Override
					public String transform(Entry<ObjectId, PrelaunchUser> input) {
						return input.getValue().getPrelaunchRegistration().getReferralToken();
					}
				}, predicate);

		if (tokens.size() > 1) {
			throw new InvalidParameterException(
					"There are " + tokens.size() + " users with referral tokens. Should be 0 or 1.");
		}

		return tokens.stream().findFirst().get();
	}

	public String getPrivateTokenByUserId(ObjectId userId) {
		Predicate predicate = Predicates.equal("id", userId);
		Collection<String> tokens = userDao.getMap()
				.project(new Projection<Map.Entry<ObjectId, PrelaunchUser>, String>() {

					@Override
					public String transform(Entry<ObjectId, PrelaunchUser> input) {
						return input.getValue().getPrelaunchRegistration().getPrivateToken();
					}
				}, predicate);

		if (tokens.size() > 1) {
			throw new InvalidParameterException(
					"There are " + tokens.size() + " users with same private tokens. Should be 0 or 1.");
		}

		return tokens.stream().findFirst().get();
	}

	public String getInviterEmailByReferralTokenAndIfActive(PrelaunchUserDao userDao, String token) {
		EntryObject e = new PredicateBuilder().getEntryObject();
		Predicate predicate = e.is("prelaunchRegistration.active")
				.and(e.get("prelaunchRegistration.referralToken").equal(token));
		Collection<String> emails = userDao.getMap()
				.project(new Projection<Map.Entry<ObjectId, PrelaunchUser>, String>() {

					@Override
					public String transform(Entry<ObjectId, PrelaunchUser> input) {
						return input.getValue().getEmail();
					}
				}, predicate);

		if (emails.size() > 1) {
			throw new InvalidParameterException(
					"There are " + emails.size() + " users with same email. Should be 0 or 1.");
		}
		Optional<String> res = emails.stream().findFirst();
		return res.isPresent() ? res.get() : null;
	}

	public String getUserEmailByPrivateToken(String privateToken) {
		Predicate predicate = Predicates.equal("prelaunchRegistration.privateToken", privateToken);

		Collection<String> emails = userDao.getMap()
				.project(new Projection<Map.Entry<ObjectId, PrelaunchUser>, String>() {

					@Override
					public String transform(Entry<ObjectId, PrelaunchUser> input) {
						return input.getValue().getEmail();
					}
				}, predicate);

		if (emails.size() > 1) {
			throw new InvalidParameterException(
					"There are " + emails.size() + " users with same emails. Should be 0 or 1.");
		}

		Optional<String> res = emails.stream().findFirst();

		return res.isPresent() ? res.get() : null;
	}

	public String getReferralTokenByUserEmail(String userEmail) {
		Predicate predicate = Predicates.equal("email", userEmail);

		Collection<String> referralTokens = userDao.getMap()
				.project(new Projection<Map.Entry<ObjectId, PrelaunchUser>, String>() {

					@Override
					public String transform(Entry<ObjectId, PrelaunchUser> input) {
						return input.getValue().getPrelaunchRegistration().getReferralToken();
					}
				}, predicate);

		if (referralTokens.size() > 1) {
			throw new InvalidParameterException(
					"There are " + referralTokens.size() + " users with same emails. Should be 0 or 1.");
		}

		Optional<String> res = referralTokens.stream().findFirst();

		return res.isPresent() ? res.get() : null;
	}

	public Collection<FriendInfo> getAllFriendsByUserEmail(String email) {
		EntryObject e = new PredicateBuilder().getEntryObject();

		PredicateBuilder predicate = e.get("prelaunchRegistration.referralEmail").equal(email);

		Collection<FriendInfo> friendEmails = userDao.getMap()
				.project(new Projection<Map.Entry<ObjectId, PrelaunchUser>, FriendInfo>() {

					@Override
					public FriendInfo transform(Entry<ObjectId, PrelaunchUser> input) {
						return new FriendInfo(input.getKey(), input.getValue().getPrelaunchRegistration().isActive(),
								input.getValue().getPrelaunchRegistration().getRegistrationDate());
					}
				}, predicate);

		return friendEmails;
	}

	public Long getNewUserCountRegisteredLastHours(long hours) {
		EntryObject e = new PredicateBuilder().getEntryObject();
		LocalDateTime ldt = LocalDateTime.now(ZoneOffset.UTC).minusHours(hours);
		Instant instant = ldt.toInstant(ZoneOffset.UTC);
		Date date = Date.from(instant);
		PredicateBuilder predicate = e.get("prelaunchRegistration.registrationDate")
				.greaterEqual(date);

		return userDao.getMap().aggregate(new UserAggregator(), predicate);
	}

}
