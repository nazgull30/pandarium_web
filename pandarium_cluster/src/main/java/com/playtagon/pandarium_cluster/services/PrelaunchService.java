package com.playtagon.pandarium_cluster.services;

import java.io.Serializable;
import java.io.StringReader;
import java.security.InvalidParameterException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.codec.digest.DigestUtils;
import org.bson.types.ObjectId;

import com.hazelcast.aggregation.Aggregator;
import com.hazelcast.map.AbstractEntryProcessor;
import com.hazelcast.projection.Projection;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import com.hazelcast.query.Predicates;
import com.playtagon.pandarium_cluster.dao.CommonSettingsDao;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.models.PrelaunchRewardParams;
import com.playtagon.pandarium_entities.CommonSettings;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration.TokenType;

@SuppressWarnings("serial")
public class PrelaunchService implements Serializable {
	private transient PrelaunchUserDao userDao;
	private transient CommonSettingsDao commonSettingsDao;

	public PrelaunchService(PrelaunchUserDao userDao, CommonSettingsDao commonSettingsDao) {
		super();
		this.userDao = userDao;
		this.commonSettingsDao = commonSettingsDao;
	}

	public PrelaunchService(PrelaunchUserDao userDao) {
		super();
		this.userDao = userDao;
	}

	public PrelaunchService(CommonSettingsDao commonSettingsDao) {
		super();
		this.commonSettingsDao = commonSettingsDao;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public long calculateTotalPandaPromo(PrelaunchRewardParams params) {
		Predicate predicate = Predicates.notEqual("prelaunchRegistration.referralEmail", null);
		TotalPromoPandasAggregator aggregator = new TotalPromoPandasAggregator(params);
		System.out.println("user count: " + userDao.count());
		Long res = userDao.getMap().aggregate(aggregator, predicate);
		return res.longValue();
	}

	public PrelaunchRewardParams getRewardParams() {
		CommonSettings prelaunchSettings = commonSettingsDao.getById("prelaunchRewardParams");
		PrelaunchRewardParams params = null;
		if (prelaunchSettings != null) {
			String xml = prelaunchSettings.getValue();

			StringReader reader = new StringReader(xml);
			try {
				JAXBContext jaxbContext = JAXBContext.newInstance(PrelaunchRewardParams.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				params = (PrelaunchRewardParams) jaxbUnmarshaller.unmarshal(reader);
			} catch (JAXBException e) {
				e.printStackTrace();
			}
		} else {
			// TEMPORARY AD-HOC!!!
			params = new PrelaunchRewardParams();
			params.setFriendCountThreshold(100);
			params.setRewardPandaCount(1);
			params.setUserCountGettingPandas(50);
		}
		return params;
	}

	@SuppressWarnings("unchecked")
	public boolean activateUser(ObjectId userId) {
		PredicateBuilder builder = new PredicateBuilder();
		EntryObject e = builder.getEntryObject();
		Predicate<ObjectId, PrelaunchUser> predicate = e.get("id").equal(userId)
				.and(e.get("prelaunchRegistration.active").equal(false));

		Collection<String> emails = userDao.getMap()
				.project(new Projection<Map.Entry<ObjectId, PrelaunchUser>, String>() {

					@Override
					public String transform(Entry<ObjectId, PrelaunchUser> input) {
						return input.getValue().getEmail();
					}
				}, predicate);

		if (emails.size() > 1) {
			throw new InvalidParameterException(
					"There are " + emails.size() + " users with same email. Should be 0 or 1.");
		}

		if (emails.size() == 0) {
			System.err.println("activateUser -> no email for user " + userId);
			return false;
		}

		String email = emails.stream().findFirst().get();

		String referralToken = generateTokenByEmail(TokenType.REFERRAL, email);
		String privateToken = generateTokenByEmail(TokenType.PRIVATE, email);
		String presaleToken = generateTokenByEmail(TokenType.PRESALE, email);

		userDao.getMap().executeOnKey(userId, new AbstractEntryProcessor<ObjectId, PrelaunchUser>() {

			@Override
			public Object process(Entry<ObjectId, PrelaunchUser> entry) {
				PrelaunchUser user = entry.getValue();
				PrelaunchUserRegistration registration = user.getPrelaunchRegistration();
				registration.setReferralToken(referralToken);
				registration.setPrivateToken(privateToken);
				registration.setPresaleToken(presaleToken);
				registration.setActive(true);
				user.setPrelaunchRegistration(registration);
				entry.setValue(user);
				return null;
			}
		});

		return true;
	}

	public static String generateTokenByEmail(TokenType tokenType, String email) {
		String token = "";
		Integer salt = ThreadLocalRandom.current().nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE);

		switch (tokenType) {
		case REFERRAL:
			token = DigestUtils.sha1Hex(email + salt.toString());
			break;
		case PRIVATE:
			token = DigestUtils.sha1Hex(salt.toString() + "private" + email);
			break;
		case PRESALE:
			token = DigestUtils.sha1Hex(salt.toString() + "presale" + email);
			break;
		case CONFIRM:
			token = DigestUtils.sha1Hex("link" + email + "jkfh39jkl");
			break;
		}
		return token;
	}

	private class TotalPromoPandasAggregator extends Aggregator<Map.Entry<ObjectId, PrelaunchUser>, Long>
			implements Serializable {

		private Map<String, Long> invitersAndReferralFriends = new HashMap<>();

		private final PrelaunchRewardParams params;

		public TotalPromoPandasAggregator(PrelaunchRewardParams params) {
			super();
			this.params = params;
		}

		@Override
		public void accumulate(Entry<ObjectId, PrelaunchUser> input) {
			String inviterEmail = input.getValue().getPrelaunchRegistration().getReferralEmail();
			long friendCount = 0;
			if (invitersAndReferralFriends.containsKey(inviterEmail)) {
				friendCount = invitersAndReferralFriends.get(inviterEmail);
			}
			invitersAndReferralFriends.put(inviterEmail, friendCount + 1);
		}

		@SuppressWarnings("rawtypes")
		@Override
		public void combine(Aggregator aggregator) {
			TotalPromoPandasAggregator pandasAggregator = (TotalPromoPandasAggregator) aggregator;
			for (Map.Entry<String, Long> entry : pandasAggregator.invitersAndReferralFriends.entrySet()) {
				if (invitersAndReferralFriends.containsKey(entry.getKey())) {
					long currentFriendCount = invitersAndReferralFriends.get(entry.getKey());
					long previFriendCount = entry.getValue();
					invitersAndReferralFriends.put(entry.getKey(), currentFriendCount + previFriendCount);
				} else {
					invitersAndReferralFriends.put(entry.getKey(), entry.getValue());
				}
			}
		}

		@Override
		public Long aggregate() {
			long totalPromoPandas = 0;
			int counter = 0;
			for (Map.Entry<String, Long> entry : invitersAndReferralFriends.entrySet()) {
				if (counter > params.getUserCountGettingPandas())
					break;
				if (entry.getValue() < params.getFriendCountThreshold())
					continue;
				totalPromoPandas += params.getRewardPandaCount();
				counter++;
			}
			return totalPromoPandas;
		}

	}

}
