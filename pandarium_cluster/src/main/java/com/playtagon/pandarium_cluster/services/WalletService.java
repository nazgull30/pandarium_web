package com.playtagon.pandarium_cluster.services;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import org.bson.json.JsonParseException;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.crypto.Wallet;
import org.web3j.crypto.WalletFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.playtagon.pandarium_cluster.dao.UserWalletDao;

public class WalletService {
	private transient UserWalletDao walletDao;

	public WalletService(UserWalletDao walletDao) {
		super();
		this.walletDao = walletDao;
	}

	private static final ObjectMapper objectMapper = new ObjectMapper();

	public static WalletFile generateNewWalletFile(String password) throws CipherException, IOException,
			InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
		ECKeyPair ecKeyPair = Keys.createEcKeyPair();
		return Wallet.createStandard(password, ecKeyPair);
	}

	public static byte[] convertWalletFileToByteArray(WalletFile walletFile) throws JsonProcessingException {
		return objectMapper.writeValueAsBytes(walletFile);
	}

	public static WalletFile convertByteArrayToWalletFile(byte[] walletBytes)
			throws JsonParseException, JsonMappingException, IOException {
		return objectMapper.readValue(walletBytes, WalletFile.class);
	}

}
