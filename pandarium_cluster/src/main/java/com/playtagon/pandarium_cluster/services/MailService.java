package com.playtagon.pandarium_cluster.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.controller.MandrillTemplatesApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import com.microtripit.mandrillapp.lutung.view.MandrillTemplate;

import connection.MailChimpConnection;
import model.list.MailChimpList;
import model.list.member.Member;
import model.list.member.MemberStatus;

public class MailService {
	private transient final String mailChimpApiKey = "fc1300510d36f1ccd4b2d1f2fbe62d0a-us10";
	private transient final String mailChimpSubscribersListId = "77838031de";
	private transient final String mailChimpOpinionLeadersListId = "d311eed2a0";

	private transient final String MANDRILL_API_KEY = "A7MQnuSmpEbYs3QyNabNzA";

	private transient final String MANDRILL_FROM_EMAIL = "dev@playtagon.com";
	private transient final String MANDRILL_FROM_NAME = "Pandarium";

	private transient final String MANDRILL_CONFRIM_TEMPLATE = "Thank you for joining!";
	private transient final String MANDRILL_ACTIVATED_TEMPLATE = "Pangratulation!";

	private transient final String MANDRILL_PRESALE_TOKEN_TEMPLATE = "Presale!";

	private transient final String MANDRILL_CONFIRM_PRESALE_ADMIN_TEMPLATE = "Confirm presale for admin";

	private transient String BASE_URL = "http://localhost:8080/#/";

	private transient Logger logger = LoggerFactory.getLogger(MailService.class);

	private transient MandrillApi mandrillApi;
	private transient MandrillTemplatesApi mandrillTemplatesApi;

	public MailService(String baseUrl) {
		BASE_URL = baseUrl;
		mandrillApi = new MandrillApi(MANDRILL_API_KEY);
		mandrillTemplatesApi = new MandrillTemplatesApi(MANDRILL_API_KEY);
	}

	public void saveToMailChimp(String email, String confirmToken) throws Exception {
		MailChimpConnection con = new MailChimpConnection(mailChimpApiKey);
		MailChimpList pandariumList = con.getList(mailChimpSubscribersListId);

		HashMap<String, Object> merge_fields_values = new HashMap<String, Object>();
		merge_fields_values.put("LINKID", confirmToken);

		pandariumList.addMember(MemberStatus.SUBSCRIBED, email, merge_fields_values);
	}

	public void addOpinionLeader(String email, String fullName, String description) throws Exception {
		MailChimpConnection con = new MailChimpConnection(mailChimpApiKey);
		MailChimpList pandariumList = con.getList(mailChimpOpinionLeadersListId);

		HashMap<String, Object> merge_fields_values = new HashMap<String, Object>();
		merge_fields_values.put("FULLNAME", fullName);
		merge_fields_values.put("DESCR", description);

		pandariumList.addMember(MemberStatus.SUBSCRIBED, email, merge_fields_values);
	}

	public boolean sendConfirmationLetter(String email, String confirmToken) {
		boolean status = false;

		logger.info("sendConfirmationLetter -> MailService.BASE_URL: " + BASE_URL);
		HashMap<String, String> mergeVariables = new HashMap<>();
		mergeVariables.put("confirm_link", BASE_URL + "confirm/" + confirmToken);

		try {
			MandrillMessage message = constructMessage("Account confirmation", MANDRILL_CONFRIM_TEMPLATE, email,
					mergeVariables);
			MandrillMessageStatus[] messageStatusReports = mandrillApi.messages().send(message, false);
			MandrillMessageStatus messageStatus = messageStatusReports[0];

			logger.info("Confirmation letter sent: " + messageStatus.getEmail() + " " + messageStatus.getStatus() + " "
					+ messageStatus.getRejectReason());

			if (messageStatus.getRejectReason() == null && messageStatus.getStatus().equals("sent"))
				status = true;
		} catch (MandrillApiError | IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return status;
	}

	public boolean sendActivateLetter(String email, String referralToken, String privateToken) {
		boolean status = false;

		logger.info("sendActivateLetter -> MailService.BASE_URL: " + BASE_URL);
		HashMap<String, String> mergeVariables = new HashMap<>();
		mergeVariables.put("private_link", BASE_URL + "progress/" + privateToken);
		mergeVariables.put("referral_link", BASE_URL + "reg/" + referralToken);

		try {
			MandrillMessage message = constructMessage("Congratulation!", MANDRILL_ACTIVATED_TEMPLATE, email,
					mergeVariables);
			MandrillMessageStatus[] messageStatusReports = mandrillApi.messages().send(message, false);
			MandrillMessageStatus messageStatus = messageStatusReports[0];

			logger.info("Activate letter sent: " + messageStatus.getEmail() + " " + messageStatus.getStatus() + " "
					+ messageStatus.getRejectReason());

			if (messageStatus.getRejectReason() == null && messageStatus.getStatus().equals("sent"))
				status = true;

		} catch (MandrillApiError | IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return status;
	}

	public boolean sendPresaleLetter(String email, String presaleToken) {
		// boolean status = false;
		//
		// HashMap<String, String> mergeVariables = new HashMap<>();
		// mergeVariables.put("presale_link", MailService.BASE_URL + "presale/" +
		// presaleToken);
		//
		// try {
		// MandrillMessage message = constructMessage("Presale!!!",
		// MailService.MANDRILL_PRESALE_TOKEN_TEMPLATE, email,
		// mergeVariables);
		// MandrillMessageStatus[] messageStatusReports =
		// mandrillApi.messages().send(message, false);
		// MandrillMessageStatus messageStatus = messageStatusReports[0];
		//
		// logger.info("Presale letter sent: " + messageStatus.getEmail() + " " +
		// messageStatus.getStatus() + " "
		// + messageStatus.getRejectReason());
		//
		// if (messageStatus.getRejectReason() == null &&
		// messageStatus.getStatus().equals("sent"))
		// status = true;
		//
		// } catch (MandrillApiError | IOException e) {
		// e.printStackTrace();
		// } catch (IllegalArgumentException e) {
		// e.printStackTrace();
		// }
		// return status;
		return true;
	}

	public boolean sendPresaleResultLetterToAdmin(int totalEmailCount, int deliveredEmailCount, String adminEmail) {
		logger.info("sendPresaleResultLetterToAdmin -> adminEmail: " + adminEmail + ", totalEmailCount: "
				+ totalEmailCount + ", deliveredEmailCount: " + deliveredEmailCount);
		// boolean status = false;
		//
		// HashMap<String, String> mergeVariables = new HashMap<>();
		// mergeVariables.put("totalEmailCount", Integer.toString(totalEmailCount));
		// mergeVariables.put("deliveredEmailCount",
		// Integer.toString(deliveredEmailCount));
		//
		// try {
		// MandrillMessage message = constructMessage("Presale mails sent",
		// MailService.MANDRILL_CONFIRM_PRESALE_ADMIN_TEMPLATE, adminEmail,
		// mergeVariables);
		// MandrillMessageStatus[] messageStatusReports =
		// mandrillApi.messages().send(message, false);
		// MandrillMessageStatus messageStatus = messageStatusReports[0];
		//
		// logger.info("Confirmation letter sent: " + messageStatus.getEmail() + " " +
		// messageStatus.getStatus() + " "
		// + messageStatus.getRejectReason());
		//
		// if (messageStatus.getRejectReason() == null &&
		// messageStatus.getStatus().equals("sent"))
		// status = true;
		// } catch (MandrillApiError | IOException e) {
		// e.printStackTrace();
		// } catch (IllegalArgumentException e) {
		// e.printStackTrace();
		// }
		// return status;
		return true;
	}

	public Member getSubscriberById(String id) throws Exception  {
		MailChimpConnection con = new MailChimpConnection(mailChimpApiKey);
		MailChimpList subscribers = con.getList(mailChimpSubscribersListId);
		return subscribers.getMember(id);
	}
	
	public List<Member> getPandariumSubscribersFromLandingPage(int count) throws Exception {
		MailChimpConnection con = new MailChimpConnection(mailChimpApiKey);
		MailChimpList subscribers = con.getList(mailChimpSubscribersListId);
		return subscribers.getMembers(count, 0);
	}

	private MandrillMessage constructMessage(String subject, String templateName, String recipientEmail,
			HashMap<String, String> mergeVariables) throws IllegalArgumentException {
		Recipient recipient = new Recipient();
		recipient.setEmail(recipientEmail);

		MandrillMessage message = new MandrillMessage();
		message.setSubject(subject);
		message.setFromEmail(MANDRILL_FROM_EMAIL);
		message.setFromName(MANDRILL_FROM_NAME);
		message.setTo(Arrays.asList(recipient));

		if (mergeVariables != null) {
			message.setMergeVars(Arrays.asList(constructMergeVarBucket(recipientEmail, mergeVariables)));
			message.setGlobalMergeVars(constructGlobalMergeVar(mergeVariables));
			message.setMerge(true);
		}

		if (templateName != null) {
			MandrillTemplate template = getTemplate(templateName);

			if (template == null) {
				logger.warn("Can't retrieve mail template with name \"" + templateName + "\"");
				throw new IllegalArgumentException();
			}

			message.setHtml(template.getCode());
		}
		return message;
	}

	private MandrillTemplate getTemplate(String templateName) {
		MandrillTemplate[] templates;
		MandrillTemplate template = null;

		try {
			templates = mandrillTemplatesApi.list();
			for (MandrillTemplate t : templates) {
				if (t.getName().equals(templateName)) {
					template = t;
					break;
				}
			}
		} catch (MandrillApiError | IOException e) {
			e.printStackTrace();
		}

		return template;
	}

	private MergeVarBucket constructMergeVarBucket(String recipientEmail, HashMap<String, String> mergeVariables) {
		MergeVarBucket mergeVarsBucket = new MergeVarBucket();
		mergeVarsBucket.setRcpt(recipientEmail);

		MergeVar[] mergeVars = new MergeVar[mergeVariables.size()];
		int i = 0;
		for (String name : mergeVariables.keySet()) {
			mergeVars[i] = new MergeVar(name, mergeVariables.get(name));
			i++;
		}
		mergeVarsBucket.setVars(mergeVars);

		return mergeVarsBucket;
	}

	private List<MergeVar> constructGlobalMergeVar(HashMap<String, String> mergeVariables) {
		List<MergeVar> mergeVars = new ArrayList<>();
		for (String name : mergeVariables.keySet()) {
			System.out.println(name + " " + mergeVariables.get(name));
			mergeVars.add(new MergeVar(name, mergeVariables.get(name)));
		}
		return mergeVars;
	}
}
