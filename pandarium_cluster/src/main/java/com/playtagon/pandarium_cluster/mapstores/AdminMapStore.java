package com.playtagon.pandarium_cluster.mapstores;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.MapLoaderLifecycleSupport;
import com.hazelcast.core.MapStore;
import com.playtagon.pandarium_cluster.utils.MapStoreUtil;
import com.playtagon.pandarium_entities.Admin;

public class AdminMapStore implements MapStore<ObjectId, Admin>, MapLoaderLifecycleSupport {
	private Datastore datastore;

	@Override
	public void init(HazelcastInstance hazelcastInstance, Properties properties, String mapName) {
		datastore = MapStoreUtil.createDatastoreByProperties(properties);
		datastore.ensureIndexes();
	}

	@Override
	public Admin load(ObjectId key) {
		return datastore.get(Admin.class, key);
	}

	@Override
	public Map<ObjectId, Admin> loadAll(Collection<ObjectId> keys) {
		List<Admin> admins = datastore.get(Admin.class, keys).asList();
		return admins.stream().collect(Collectors.toMap(Admin::getId, e -> e));
	}

	@Override
	public Iterable<ObjectId> loadAllKeys() {
		return datastore.createQuery(Admin.class).asList().stream().map(e -> e.getId()).collect(Collectors.toList());
	}

	@Override
	public void store(ObjectId key, Admin value) {
		datastore.save(value);
	}

	@Override
	public void storeAll(Map<ObjectId, Admin> map) {
		datastore.save(map.entrySet());
	}

	@Override
	public void delete(ObjectId key) {
		datastore.delete(Admin.class, key);

	}

	@Override
	public void deleteAll(Collection<ObjectId> keys) {
		datastore.delete(Admin.class, keys);

	}

	@Override
	public void destroy() {

	}

}
