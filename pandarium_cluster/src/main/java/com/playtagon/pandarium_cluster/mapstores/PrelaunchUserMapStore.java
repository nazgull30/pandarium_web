package com.playtagon.pandarium_cluster.mapstores;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.MapLoaderLifecycleSupport;
import com.hazelcast.core.MapStore;
import com.playtagon.pandarium_cluster.utils.MapStoreUtil;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration;

public class PrelaunchUserMapStore implements MapStore<ObjectId, PrelaunchUser>, MapLoaderLifecycleSupport {
	private Datastore datastore;

	@Override
	public void init(HazelcastInstance hazelcastInstance, Properties properties, String mapName) {
		datastore = MapStoreUtil.createDatastoreByProperties(properties);
		datastore.ensureIndexes();
	}

	@Override
	public PrelaunchUser load(ObjectId key) {
		return datastore.get(PrelaunchUser.class, key);
	}

	@Override
	public Map<ObjectId, PrelaunchUser> loadAll(Collection<ObjectId> keys) {
		List<PrelaunchUser> users = datastore.get(PrelaunchUser.class, keys).asList();
		return users.stream().collect(Collectors.toMap(PrelaunchUser::getId, e -> e));
	}

	@Override
	public Iterable<ObjectId> loadAllKeys() {
		return datastore.createQuery(PrelaunchUser.class).asList().stream().map(e -> e.getId())
				.collect(Collectors.toList());
	}

	@Override
	public void store(ObjectId key, PrelaunchUser value) {
		datastore.save(value);
	}

	@Override
	public void storeAll(Map<ObjectId, PrelaunchUser> map) {
		List<PrelaunchUserRegistration> registrations = map.values().stream().map(e -> e.getPrelaunchRegistration())
				.collect(Collectors.toList());
		datastore.save(registrations);
		datastore.save(map.entrySet());
	}

	@Override
	public void delete(ObjectId key) {
		datastore.delete(PrelaunchUser.class, key);

	}

	@Override
	public void deleteAll(Collection<ObjectId> keys) {
		datastore.delete(PrelaunchUser.class, keys);

	}

	@Override
	public void destroy() {

	}

}
