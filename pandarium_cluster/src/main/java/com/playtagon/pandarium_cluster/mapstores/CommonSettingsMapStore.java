package com.playtagon.pandarium_cluster.mapstores;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.mongodb.morphia.Datastore;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.MapLoaderLifecycleSupport;
import com.hazelcast.core.MapStore;
import com.playtagon.pandarium_cluster.utils.MapStoreUtil;
import com.playtagon.pandarium_entities.CommonSettings;
import com.playtagon.pandarium_entities.PrelaunchUser;

public class CommonSettingsMapStore implements MapStore<String, CommonSettings>, MapLoaderLifecycleSupport {
	private Datastore datastore;

	@Override
	public void init(HazelcastInstance hazelcastInstance, Properties properties, String mapName) {
		datastore = MapStoreUtil.createDatastoreByProperties(properties);
		datastore.ensureIndexes();
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public CommonSettings load(String key) {
		return datastore.get(CommonSettings.class, key);
	}

	@Override
	public Map<String, CommonSettings> loadAll(Collection<String> keys) {
		List<CommonSettings> settings = datastore.get(CommonSettings.class, keys).asList();
		return settings.stream().collect(Collectors.toMap(CommonSettings::getKey, e -> e));
	}

	@Override
	public Iterable<String> loadAllKeys() {
		return datastore.createQuery(CommonSettings.class).asList().stream().map(e -> e.getKey())
				.collect(Collectors.toList());
	}

	@Override
	public void store(String key, CommonSettings value) {
		datastore.save(value);
	}

	@Override
	public void storeAll(Map<String, CommonSettings> map) {
		datastore.save(map.entrySet());
	}

	@Override
	public void delete(String key) {
		datastore.delete(PrelaunchUser.class, key);
	}

	@Override
	public void deleteAll(Collection<String> keys) {
		datastore.delete(PrelaunchUser.class, keys);
	}

}
