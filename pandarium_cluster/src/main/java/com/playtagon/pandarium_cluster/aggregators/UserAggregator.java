package com.playtagon.pandarium_cluster.aggregators;

import java.util.Map;
import java.util.Map.Entry;

import org.bson.types.ObjectId;

import com.hazelcast.aggregation.Aggregator;
import com.playtagon.pandarium_entities.PrelaunchUser;

@SuppressWarnings("serial")
public class UserAggregator extends Aggregator<Map.Entry<ObjectId, PrelaunchUser>, Long> {
	private long count;

	@Override
	public void accumulate(Entry<ObjectId, PrelaunchUser> input) {
		count++;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void combine(Aggregator aggregator) {
		UserAggregator friendsAggregator = (UserAggregator) aggregator;
		count += friendsAggregator.count;
	}

	@Override
	public Long aggregate() {
		return count;
	}

}