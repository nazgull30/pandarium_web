package com.playtagon.pandarium_cluster.utils.databasemigration;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.playtagon.pandarium_cluster.utils.databasemigration.DatabaseMigrationTool;
import com.playtagon.pandarium_entities.PrelaunchUser;

public class DatabaseMigrationToolTest {
	private final int oldUserCont = 25;
	private final int newUserCont = 75;

	private final String sameEmail = "nazgull30@gmail.com";
	private DatabaseMigrationTool tool;

	@Before
	public void setUp() throws FileNotFoundException {
		tool = new DatabaseMigrationTool();
		tool.init();

		HazelcastInstance newCluster = tool.getNewCluster();
		HazelcastInstance oldCluster = tool.getOldCluster();

		PrelaunchUser sameUser = new PrelaunchUser(new ObjectId(), sameEmail, null);

		IMap<ObjectId, PrelaunchUser> oldPrelaunchUsers = oldCluster.getMap("prelaunchUsers");
		IMap<ObjectId, PrelaunchUser> newPrelaunchUsers = newCluster.getMap("prelaunchUsers");

		oldPrelaunchUsers.set(sameUser.getId(), sameUser);
		newPrelaunchUsers.set(sameUser.getId(), sameUser);

		for (int i = 0; i < oldUserCont; i++) {
			PrelaunchUser randomUser = new PrelaunchUser(new ObjectId(), getRandomEmail(), null);
			oldPrelaunchUsers.set(randomUser.getId(), randomUser);
		}

		for (int i = 0; i < newUserCont; i++) {
			PrelaunchUser randomUser = new PrelaunchUser(new ObjectId(), getRandomEmail(), null);
			newPrelaunchUsers.set(randomUser.getId(), randomUser);
		}

		tool.printClustersInfo();
	}

	@After
	public void tearDown() {
		tool.shutdown();
	}

	@Test
	public void testCheckSameEmails() {
		List<String> sameEmails = tool.checkSameEmails();
		Assert.assertEquals(sameEmails.size(), 1);
		Assert.assertTrue(sameEmails.contains(sameEmail));
	}

	@Test
	public void testMigratePrelaunchUsers() {
		tool.migratePrelaunchUsers();
		IMap<ObjectId, PrelaunchUser> newPrelaunchUsers = tool.getNewCluster().getMap("prelaunchUsers");
		Collection<PrelaunchUser> newUsers = newPrelaunchUsers.values();

		Assert.assertEquals(newPrelaunchUsers.size(), oldUserCont + newUserCont + 1);
		Assert.assertEquals(newUsers.stream().filter(u -> u.getEmail().equals(sameEmail)).count(), 1);
	}

	private String getRandomEmail() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 10) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr + "@gmail.com";
	}

}
