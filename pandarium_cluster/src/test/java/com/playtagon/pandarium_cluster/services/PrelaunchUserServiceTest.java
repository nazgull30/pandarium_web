package com.playtagon.pandarium_cluster.services;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration;

public class PrelaunchUserServiceTest {
	private static HazelcastInstance hz;

	@BeforeClass
	public static void setUp() throws Exception {
		InputStream is = ClassLoader.getSystemResourceAsStream("hazelcast_test.xml");
		Config config = new XmlConfigBuilder(is).build();
		hz = Hazelcast.newHazelcastInstance(config);
		HazelcastUtil.getInstance().setHazelcastInstance(hz);
	}

	@After
	public void after() {
		hz.getMap("prelaunchUsers").clear();
	}

	@Test
	public void testGetNewUserCountRegisteredLastHours() {
		IMap<ObjectId, PrelaunchUser> users = hz.getMap("prelaunchUsers");

		PrelaunchUserRegistration registration1 = new PrelaunchUserRegistration();
		registration1.setRegistrationDate(new Date());
		PrelaunchUser user1 = new PrelaunchUser();
		user1.setId(new ObjectId());
		user1.setPrelaunchRegistration(registration1);
		users.set(user1.getId(), user1);

		PrelaunchUserRegistration registration2 = new PrelaunchUserRegistration();
		Date date2 = convertLastHoursToDate(30);
		registration2.setRegistrationDate(date2);
		PrelaunchUser user2 = new PrelaunchUser();
		user2.setId(new ObjectId());
		user2.setPrelaunchRegistration(registration2);
		users.set(user2.getId(), user2);

		PrelaunchUserRegistration registration3 = new PrelaunchUserRegistration();
		Date date3 = convertLastHoursToDate(20);
		registration3.setRegistrationDate(date3);
		PrelaunchUser user3 = new PrelaunchUser();
		user3.setId(new ObjectId());
		user3.setPrelaunchRegistration(registration3);
		users.set(user3.getId(), user3);

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		PrelaunchUserService userService = new PrelaunchUserService(userDao);

		long userCount = userService.getNewUserCountRegisteredLastHours(24);

		assertEquals(userCount, 2);
	}

	private Date convertLastHoursToDate(long hours) {
		LocalDateTime ldt = LocalDateTime.now(ZoneOffset.UTC).minusHours(hours);
		Instant instant = ldt.toInstant(ZoneOffset.UTC);
		return Date.from(instant);
	}
}
