package com.playtagon.pandarium_cluster.tasks;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IExecutorService;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.models.EmailData;
import com.playtagon.pandarium_cluster.tasks.GetUserEmailsTask;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration;

public class GetUserEmailsTaskTest {
	private final String inviterEmail = "nazgull30@gmail.com";

	private HazelcastInstance hz;

	@Before
	public void setUp() throws Exception {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("hazelcast_test.xml");
		Config config = new XmlConfigBuilder(is).build();
		hz = Hazelcast.newHazelcastInstance(config);
		HazelcastUtil.getInstance().setHazelcastInstance(hz);

		PrelaunchUserRegistration inviterReg = new PrelaunchUserRegistration(new ObjectId(), "");
		inviterReg.setActive(true);
		PrelaunchUser inviter = new PrelaunchUser(new ObjectId(), inviterEmail, inviterReg);

		PrelaunchUserRegistration invitee1Reg = new PrelaunchUserRegistration(new ObjectId(), "");
		invitee1Reg.setReferralEmail(inviterEmail);
		invitee1Reg.setActive(true);
		PrelaunchUser invitee1 = new PrelaunchUser(new ObjectId(), "invitee1@gmail.con", invitee1Reg);

		PrelaunchUserRegistration invitee2Reg = new PrelaunchUserRegistration(new ObjectId(), "");
		invitee2Reg.setReferralEmail(inviterEmail);
		PrelaunchUser invitee2 = new PrelaunchUser(new ObjectId(), "invitee2@gmail.con", invitee2Reg);

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		userDao.save(inviter.getId(), inviter);
		userDao.save(invitee1.getId(), invitee1);
		userDao.save(invitee2.getId(), invitee2);
	}

	@After
	public void after() {
		hz.getMap("users").clear();
		hz.getMap("prelaunchUsers").clear();
		hz.getMap("admins").clear();
		hz.getMap("common_settings").clear();
		hz.shutdown();
	}

	@Test
	public void testGetEmailData() throws InterruptedException, ExecutionException {
		IExecutorService executorService = hz.getExecutorService("default");
		Future<List<EmailData>> future = executorService.submit(new GetUserEmailsTask());
		List<EmailData> emails = future.get();

		Assert.assertEquals(emails.size(), 3);

		for (EmailData emailData : emails) {
			System.out.println("Test -> " + emailData.getEmail() + ", totalFriends: " + emailData.getTotalFriends()
					+ ", verifiedFriends: " + emailData.getVerifiedFriends());
		}

		long emailsWithTotalFriends = emails.stream().filter(e -> e.getTotalFriends() == 2).count();
		long emailWithVerifiedFriends = emails.stream().filter(e -> e.getVerifiedFriends() == 1).count();

		Assert.assertEquals(emailsWithTotalFriends, 1);
		Assert.assertEquals(emailWithVerifiedFriends, 1);
	}

	@Test
	public void testGetAllUserEmails() {
		GetUserEmailsTask emailsTask = new GetUserEmailsTask();
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		emailsTask.setHazelcastInstance(hz);

		Collection<EmailData> emailDatas = emailsTask.getAllUserEmails(userDao);
		Assert.assertEquals(emailDatas.size(), 3);
	}

	@Test
	public void testGetUserFriendsCount() {
		GetUserEmailsTask emailsTask = new GetUserEmailsTask();
		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);
		emailsTask.setHazelcastInstance(hz);

		long totalFriendCount = emailsTask.getUserFriendsCount(inviterEmail, false, userDao);
		long activeFriendCount = emailsTask.getUserFriendsCount(inviterEmail, true, userDao);

		Assert.assertEquals(totalFriendCount, 2);
		Assert.assertEquals(activeFriendCount, 1);
	}

}
