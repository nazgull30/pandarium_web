package com.playtagon.pandarium_cluster.tasks;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.awaitility.Awaitility;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IExecutorService;
import com.hazelcast.core.IMap;
import com.playtagon.pandarium_cluster.dao.PrelaunchUserDao;
import com.playtagon.pandarium_cluster.services.MailService;
import com.playtagon.pandarium_cluster.utils.HazelcastUtil;
import com.playtagon.pandarium_cluster.utils.Utils;
import com.playtagon.pandarium_entities.PrelaunchUser;
import com.playtagon.pandarium_entities.PrelaunchUserRegistration;

import model.list.member.Member;

public class CreatePrelaunchRegistrationForUsersFromLandingTaskTest {
	private HazelcastInstance hz;

	@Before
	public void setUp() throws Exception {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("hazelcast_test.xml");
		Config config = new XmlConfigBuilder(is).build();
		hz = Hazelcast.newHazelcastInstance(config);
		HazelcastUtil.getInstance().setHazelcastInstance(hz);

	}

	@After
	public void after() {
		hz.getMap("users").clear();
		hz.getMap("prelaunchUsers").clear();
		hz.getMap("admins").clear();
		hz.getMap("common_settings").clear();
		hz.shutdown();
	}

	@Test
	public void testTask() throws Exception {
		int retrieveEmailCount = 10;

		String baseUrl = Utils.getBaseUrl(hz);
		MailService mailService = new MailService(baseUrl);
		List<Member> subscribers = mailService.getPandariumSubscribersFromLandingPage(retrieveEmailCount);

		Awaitility.await().until(() -> subscribers.size() == retrieveEmailCount);

		PrelaunchUserDao userDao = new PrelaunchUserDao(hz);

		List<ObjectId> registeredIds = new ArrayList<>();

		// Two users from landing page registered after prelaunch
		int registeredUserCount = 2;
		for (int i = 0; i < registeredUserCount; i++) {
			String email = subscribers.get(i).getEmail_address();
			System.out.println("Test: create user with email " + email);
			ObjectId id = new ObjectId();
			userDao.save(id, new PrelaunchUser(id, email, new PrelaunchUserRegistration(new ObjectId(), "")));
			registeredIds.add(id);
		}

		for (PrelaunchUser prelaunchUser : userDao.getAllEntites()) {
			System.out.println("User: " + prelaunchUser.getId() + ", email: " + prelaunchUser.getEmail());
		}

		IExecutorService executorService = hz.getExecutorService("default");
		executorService.execute(new CreatePrelaunchUserEntitiesFromLandingAndNotifyTask(retrieveEmailCount, false));

		Thread.sleep(8000);

		IMap<ObjectId, PrelaunchUser> prelaunchUsers = hz.getMap("prelaunchUsers");
		for (PrelaunchUser user : prelaunchUsers.values()) {
			Assert.assertFalse(user.getPrelaunchRegistration().isActive());
			Assert.assertNull(user.getPrelaunchRegistration().getPrivateToken());
			if (!registeredIds.contains(user.getId())) {
				System.out.println("Check email: " + user.getEmail());
				Assert.assertNotNull(user.getPrelaunchRegistration().getConfirmToken());
			}
		}

		System.out.println("size:" + prelaunchUsers.size());
	}

}
