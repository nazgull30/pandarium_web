package com.playtagon.pandarium_entities;

import java.io.Serializable;
import java.util.Date;

import org.bson.types.ObjectId;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class PrelaunchUserRegistration implements Comparable<PrelaunchUserRegistration>, Serializable {
	private static final long serialVersionUID = 3216670687699868160L;

	private ObjectId id;
	private String ip; // ip used to registration
	private String referralEmail; // user who sent a referral link
	private String referralToken; // token used in referral link
	private String privateToken; // token used in link to user's friend progress information
	private String presaleToken; // token used in link to user's presale information
	private boolean presaleSent;
	private String confirmToken; // token used for account confirmation
	private boolean active; // user confirmation status;
	private Date registrationDate;

	public PrelaunchUserRegistration(ObjectId id, String referralEmail) {
		super();
		this.id = id;
		this.referralEmail = referralEmail;
	}

	public PrelaunchUserRegistration(ObjectId id) {
		super();
		this.id = id;
	}

	@Override
	public int compareTo(PrelaunchUserRegistration o) {
		return id.compareTo(o.getId());
	}

	public static enum TokenType {
		REFERRAL, PRIVATE, PRESALE, CONFIRM
	}
}
