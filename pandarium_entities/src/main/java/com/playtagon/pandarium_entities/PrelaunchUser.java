package com.playtagon.pandarium_entities;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Entity("prelaunch_users")
@Data @NoArgsConstructor
public class PrelaunchUser implements Serializable {
	@Id
	private ObjectId id;
	private String email;
	@Embedded
	private PrelaunchUserRegistration prelaunchRegistration;

	public PrelaunchUser(ObjectId id, String email, PrelaunchUserRegistration prelaunchRegistration) {
		super();
		this.id = id;
		this.email = email;
		this.prelaunchRegistration = prelaunchRegistration;
	}

	public PrelaunchUser(String email, PrelaunchUserRegistration prelaunchRegistration) {
		super();
		this.email = email;
		this.prelaunchRegistration = prelaunchRegistration;
	}

}
