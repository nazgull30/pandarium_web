package com.playtagon.pandarium_entities;

import java.io.IOException;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity("common_settings")
@Data @AllArgsConstructor @NoArgsConstructor
public class CommonSettings implements DataSerializable {
	@Id
	private String key;
	private String value;

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeUTF(key);
		out.writeUTF(value);
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		key = in.readUTF();
		value = in.readUTF();
	}

}
