package com.playtagon.pandarium_entities;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity("wallets")
@Data @AllArgsConstructor
public class UserWallet {
	@Id
	private String address;
	private byte[] data;
}
