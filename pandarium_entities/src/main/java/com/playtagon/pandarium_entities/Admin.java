package com.playtagon.pandarium_entities;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Entity("admins")
@Data @NoArgsConstructor
public class Admin implements Serializable {
	@Id
	private ObjectId id;
	private String username;
	private String hashedPassword;
	private byte[] passwordSalt;

	public Admin(String username, String hashedPassword, byte[] passwordSalt) {
		super();
		this.username = username;
		this.hashedPassword = hashedPassword;
		this.passwordSalt = passwordSalt;
	}

}
